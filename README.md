# Temporal variability and cell mechanics control robustness in mammalian embryogenesis

## Updated version
An up-to-date version of this repository can be found at: https://gitlab.com/fabreges/fabreges-2024

## Authors
Dimitri Fabrèges<sup>1,#,\*</sup>, Bernat Corominas Murtra<sup>2,\*</sup>, Prachiti Moghe<sup>1,#</sup>,
Alison Kickuth<sup>1</sup>, Takafumi Ichikawa<sup>3,4</sup>, Chizuru Iwatani<sup>5</sup>,
Tomoyuki Tsukiyama<sup>3,5</sup>, Nathalie Daniel<sup>6</sup>, Julie Gering<sup>7</sup>,
Anniek Stokkermans<sup>7</sup>, Adrian Wolny<sup>8</sup>, Anna Kreshuk<sup>8</sup>, Véronique Duranthon<sup>6</sup>,
Virginie Uhlmann<sup>9</sup>, Edouard Hannezo<sup>10,\*</sup>, Takashi Hiiragi<sup>1,3,4,#,\*</sup>

- <sup>1</sup>Developmental Biology Unit, European Molecular Biology Laboratory, 69117, Heidelberg, Germany
- <sup>2</sup>Institute of Biology, University of Graz, 8010 Graz, Austria
- <sup>3</sup>Institute for the Advanced Study of Human Biology (WPI-ASHBi), Kyoto University, Kyoto, 606-8501, Japan
- <sup>4</sup>Department of Developmental Biology, Graduate School of Medicine, Kyoto University, Kyoto, 606-8501, Japan
- <sup>5</sup>Research Center for Animal Life Science, Shiga University of Medical Science, Shiga, 520-2192, Japan
- <sup>6</sup>UVSQ, INRAE, BREED, Université Paris-Saclay, 78350, Jouy-en-Josas, France
- <sup>7</sup>Hubrecht Institute, 3584 CT Utrecht, Netherland
- <sup>8</sup>Cell Biology and Biophysics Unit, European Molecular Biology Laboratory, 69117, Heidelberg, Germany
- <sup>9</sup>EMBL-EBI, Wellcome Genome Campus, Hinxton, CB10 1SD, UK
- <sup>10</sup>Institute of Science and Technology Austria, 3400 Klosterneuburg, Austria
- <sup>#</sup>Current address: Hubrecht Institute, 3584 CT Utrecht, Netherland
- <sup>\*</sup>Correspondence: [d.fabreges@hubrecht.eu](mailto:d.fabreges@hubrecht.eu); [bernat.corominas-murtra@uni-graz.at](mailto:bernat.corominas-murtra@uni-graz.at); [edouard.hannezo@ist.ac.at](mailto:edouard.hannezo@ist.ac.at); [t.hiiragi@hubrecht.eu](mailto:t.hiiragi@hubrecht.eu)

## Abstract
How living systems achieve precision in form and function despite their intrinsic stochasticity is a fundamental yet
open question in biology. Here, we establish a quantitative morphomap of pre-implantation embryogenesis in mouse,
rabbit and monkey embryos, which reveals that although blastomere divisions desynchronise passively without
compensation, 8-cell embryos still display robust 3D structure. Using topological analysis and genetic perturbations in
mouse, we show that embryos progressively change their cellular connectivity to a preferred topology, which can be
predicted by a simple physical model where noise and actomyosin-driven compaction facilitate topological transitions
lowering surface energy. This favours the most compact embryo packing at the 8- and 16-cell stage, thus promoting
higher number of inner cells. Impairing mitotic desynchronisation reduces embryo packing compactness and generates
significantly more cell mis-allocation and a lower proportion of inner-cell-mass-fated cells, suggesting that
stochasticity in division timing contributes to achieving robust patterning and morphogenesis.

## This repository
In this repository, you will find the R-scripts used to generate the figures, along with the data necessary to make
the plots. You will also find the Python-scripts used to compute the pair-wise distance between embryos, to estimate
the surface, the volume, the α-parameter, to identify the closest rigid packing and to generate the morphomap. Additionally,
you have access to the base model used with Plantseg to predict and label the cells at the 8-cell stage. Note that a more
accurate and more specific model was generated for each embryo based on the curation of at least 5 timepoints.

### Figures
The plots in Figure 1-6 and Figure S1-S7 can be generated with the respective files. The code is written in R and
requires RStudio to work out-of-the-box. The R scripts are located in `figures/`. Figures and supplemental figures
are generated with the same file (e.g., `Figure 1.R` generates all the computed panels of Figure 1 and Figure S1).
Data required to generate the plots are located in `figures/data/`. Note that the code to work with the morphomap
and the tracking data is located in local packages `figures/fabreges.morphomap` and `figures/fabreges.tracking`. An
additional package `figures/fabreges.generic` contains utility functions.

### Python packages
For the needs of this study, we developed a series of package for Python located in `python-packages/`. The packages
can be directly imported within your scripts. Along with the packages, you will find 6 scripts used to compute,
extract or estimate the data from the source. The folder is organised as follow:

- package `interfaces` is located in `python-packages/interfaces/` and can be tested by running the scripts to extract
the angles and estimate the surface of contact (`compute-angles.py` and `estimate-contacts.py` respectively) in folder
`python-packages`,
- package `morphomap` is located in `python-packages/morphomap/` and can be tested by running the script to compute
the geometrical distances between embryos, project the data in 2D and compute the volume of the cells
(`compute-distances.py`, `process-distances.py` and `compute-volumes.py` respectively) in folder `python-packages`,
- package `splinefit` is included in `morphomap` in `python-packages/morphomap/splinefit/`, but can function
independently,
- package `rbb` is located in `python-packages/rbb/` and can be tested by running the script to identify the closest
rigid packing of an embryo at the 8-cell stage (`identify-packing.py`) in folder `python-packages`.

We provide example of segmented data in `python-packages/samples/`.

### CNN
We provide a convolutional neural network (CNN) used in our study, located in `cnn`. Please note that we optimised the
CNN further for each dataset by doing manual curation of at least 5 time points and by retraining the network with those
ground truth. The CNN were trained using [pytorch-3dunet](https://github.com/wolny/pytorch-3dunet).
The CNN can be used with [Plantseg](https://github.com/hci-unihd/plant-seg)[^1]. Currently (plantseg v.1.6.0 and
pytorch-3dunet v.1.6.0), the output model from `pytorch-3dunet` must currently be modified to work with plantseg:

```py
import torch

for model in ('best_checkpoint.pytorch', 'last_checkpoint.pytorch'):
    state = torch.load(model, map_location='cpu')
    state = state['model_state_dict']
    torch.save(state, model)
```

## Requierements
### Figures
The figures were generated with `R` (v. 4.4.0) and `RStudio` (v. 2024.04.1+748). The following
packages must be installed prior to execution:

 - `alphashape3d` (v. 1.3.2)
 - `devtools` (v. 2.4.5)
 - `ggplot2` (v. 3.5.1)
 - `ggrepel` (v. 0.9.5)
 - `rstudioapi` (v. 0.16.0)

To install all these packages, run the following command in the RStudio console:

```r
install.packages(c("alphashape3d@1.3.2", "devtools@2.4.5", "ggplot2@3.5.1", "ggrepel@0.9.5", "rstudioapi@0.16"))
```

More recent version of R, RStudio and the packages may also work (not tested).

### Python packages
Python 3.12.3 was used to analyse the data. The scripts may work with more recent version of Python
and with Python 3.8 or earlier (not tested). To run the code, you must first install the following
packages:

 - `numpy` (v. 1.26.4)
 - `imageio` (v. 2.34.1)
 - `scipy` (v. 1.13.1)
 - `scikit-image` (v. 0.23.2)
 - `scikit-learn` (v. 1.5.0)

Alternatively, you can create a `conda` environment:

```bash
conda create -y --name fabreges2024 python=3.12.3
conda activate fabreges2024
python -m pip install numpy==1.26.4 imageio==2.34.1 scipy==1.13.1 scikit-image==0.23.2 scikit-learn==1.5.0
```

More recent version of the dependencies may work (not tested).

## Usage
### Figures
Open one of the figure script in RStudio and run the entire code. You may have to create a folder `output`
in `figures/` if it does not exist.

The code has local dependencies that will be installed on your computer when encountering them for the first time.
They start with `fabreges.` and are necessary to generate the figures.

### Python packages (example)
#### Compute angles

```bash
# Compute a summary (mean, median, s.d.) of the angles in embryo C1 at time 40
python compute-angles.py -s -r 15 -R 20 samples/C1_t040.tif.gz output/C1_t040_angles.csv

# Compute a detailed list of angle values
python compute-angles.py -r 15 -R 20 samples/C1_t041.tif.gz output/C1_t041_angles.csv
```

#### Compute distances

```bash
# Compute the distance between two embryos
python compute-distances.py -c 5 -s 5 samples/C1_t040.tif.gz samples/C1_t041.tif.gz output/distances.csv

# Append another distance measurement
python compute-distances.py -a -c 5 -s 5 samples/C1_t040.tif.gz samples/C1_t042.tif.gz output/distances.csv
```

#### Compute volumes

```bash
# Compute the volume in isotropic data
python compute-volumes.py -r 0.416 0.416 0.416 samples/C1_t040.tif.gz output/volumes.csv
```

#### Estimate contacts

```bash
# Estimate the contacts and the free surface area
python estimate-contacts.py -r 0.416 0.416 0.416 samples/C1_t040.tif.gz output/contacts.csv
```

#### Identify the closest rigid packing

```bash
# Assuming the file `output/contacts.csv` exists and contains the output
# from command `estimate-contacts.py`.

# Identify the closest rigid packings
python identify-packing.py output/contacts.csv output/backbones.csv
```

#### Generate the seeds for Surface Evolver

```bash
# Assuming the file `output/contacts.csv` exists and contains the output
# from command `estimate-contacts.py`, and the file `output/volumes.csv`
# exists and contains the output from command `compute-volumes.py`

# Generate the seeds for Surface Evolver using automatic thresholding
# Automatic threshold will determine the highest cell-cell contact threshold
# that preserves rigidity of the embryo (or 0 if not rigid)
python generate-seed.py output/contacts.csv output/volumes.csv output/seed.fe

# Generate the seeds for Surface Evolver with custom threshold
python generate-seed.py --threshold 0.1 output/contacts.csv output/volumes.csv output/seed.fe
```

#### Project in 2D and generate a distance matrix

```bash
# Assuming the folder `foo` only contains one or more CSV
# from command `compute-distances.py`.

# Compute the tSNE 2D projection
python process-distances.py -D2 -t output/tsne_2d.csv foo/

# Compute the tSNE 3D projection
python process-distances.py -D3 -t output/tsne_2d.csv foo/

# Generate the distance matrix
python process-distances.py -m output/dismat.csv foo/
```

[^1]: Wolny, A., Cerrone, L., Vijayan, A., Tofanelli, R., Barro, A.V., Louveaux, M., Wenzl, C., Strauss, S., Wilson-Sánchez, D., Lymbouridou, R., et al. (2020). Accurate and versatile 3D segmentation of plant tissues at cellular resolution. **eLife** 9, 1–34. [10.7554/eLife.57613](https://doi.org/10.7554/eLife.57613).
