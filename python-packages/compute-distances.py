#! /usr/bin/env python
import argparse
import morphomap

import os
import sys
import time

from shared import logger

parser = argparse.ArgumentParser(description = "Compute the pair-wise distances between clusters of 8 objects")
parser.add_argument("-c", "--count",    type   = int,          default = 5,     help = "Number of longitude and latitude. See Ms and Mt.")
parser.add_argument("-s", "--sampling", type   = int,          default = 5,     help = "Number of sampling points in-between longitude and latitude. See Rs and Rt.")
parser.add_argument("-a", "--append",   action = "store_true", default = False, help = "Whether to append the result to the output file, or to overwrite the destination file (default).")
parser.add_argument("input",            type   = str,          nargs   = 2,     help = "Two TIF or TIF.GZ files to measure the distance.")
parser.add_argument("output",           type   = str,                           help = "The destination file.")
args = parser.parse_args()

logger.info("-- DISTANCE MEASUREMENT --")
logger.info(f"Number of longitude: {args.count}")
logger.info(f"Longitude sampling:  {args.sampling}")
logger.info(f"Number of latitude:  {args.count}")
logger.info(f"Latitude sampling:   {args.sampling}")
logger.info(f"First input file:    {args.input[0]}")
logger.info(f"Second input file:   {args.input[1]}")
logger.info(f"Output to file:      {args.output}")
logger.info(f"Append to output:    {args.append}")

for inp in args.input:
    if not os.path.isfile(inp) or not os.access(inp, os.R_OK):
        logger.error(f"Input file does not exist or is not reachable: {inp}")
        sys.exit(1)

mm = morphomap.Morphomap()
v1 = mm.add_volume(args.input[0])
v2 = mm.add_volume(args.input[1])
mm.add_link(v1, v2)

mm.compute_distances(args.count, args.sampling, low_memory = True)

if not args.append:
    with open(args.output, "w") as fw:
        fw.write(f"file_A,file_B,distance\n")

with open(args.output, "a") as fa:
    for d in mm.distances:
        vA  = d[0]
        vB  = d[1]
        dis = d[2]

        fa.write(f"{vA.filepath},{vB.filepath},{dis}\n")

