import gzip
import imageio.v3 as imageio
import numpy
import os

class Volume:

    def __init__(self, filepath):
        self._filepath  = filepath
        self.loaded     = None
        self.data       = None
        self.metadata   = None


    @property
    def filepath(self):
        return self._filepath


    def load(self):
        if self.loaded == self._filepath:
            return self.data

        self.metadata = {}
        compressed    = False

        try:
            with gzip.open(self._filepath, 'rb') as fr:
                data       = fr.read()
                compressed = True

        except gzip.BadGzipFile:
            with open(self._filepath, 'rb') as fr:
                data = fr.read()

        self.data   = imageio.imread(data, extension = ".tif")
        self.loaded = self._filepath

        self.metadata['io'] = {"compressed": compressed, "read_from": self._filepath}
        return self.data


    def free(self):
        self.data     = None
        self.loaded   = None
        self.metadata = None


    def __eq__(self, other):
        return isinstance(other, Volume) and other.filepath == self._filepath


    def __hash__(self):
        return hash(self.filepath)


    def __format__(self, spec):
        return format(self.filepath, spec)


class LabelledVolume(Volume):

    def __init__(self, filepath):
        super().__init__(filepath)


    def load(self):
        if self.loaded == self.filepath:
            return self.data

        super().load()
        self.metadata['ids']       = []
        self.metadata['volumes']   = []
        self.metadata['centroids'] = []
        self.metadata['boxes']     = []
        self.metadata['count']     = 0

        ids, counts = numpy.unique(self.data, return_counts = True)
        for i in range(len(ids)):
            idx = ids[i]
            if idx == 0:
                idx = -1
                obj = numpy.where(self.data > 0)
            else:
                obj = numpy.where(self.data == idx)

            volume   = len(obj[0])
            centroid = numpy.mean(obj, axis = 1)
            box      = [*numpy.min(obj, axis = 1), *numpy.max(obj, axis = 1)]

            self.metadata['ids']       += [idx]
            self.metadata['volumes']   += [volume]
            self.metadata['centroids'] += [centroid]
            self.metadata['boxes']     += [box]
            self.metadata['count']     += 1 if idx > 0 else 0


    def ids(self):
        if not self.loaded:
            raise ValueError(f"LabelledVolume: data are not loaded yet: {self.filepath}")

        return numpy.array([self.metadata['ids'][i]
                                for i in range(len(self.metadata['ids']))
                                if self.metadata['ids'][i] >= 0])


    def count(self):
        if not self.loaded:
            raise ValueError(f"LabelledVolume: data are not loaded yet: {self.filepath}")

        return self.metadata['count']


    def volumes(self):
        if not self.loaded:
            raise ValueError(f"LabelledVolume: data are not loaded yet: {self.filepath}")

        return numpy.array([self.metadata['volumes'][i]
                                for i in range(len(self.metadata['ids']))
                                if self.metadata['ids'][i] >= 0])


    def centroids(self, order = 'xyz'):
        if not self.loaded:
            raise ValueError(f"LabelledVolume: data are not loaded yet: {self.filepath}")

        o = -1 if order == 'xyz' else 1
        return numpy.array([self.metadata['centroids'][i][::o]
                                for i in range(len(self.metadata['ids']))
                                if self.metadata['ids'][i] >= 0])


    def boxes(self, order = 'xyz'):
        if not self.loaded:
            raise ValueError(f"LabelledVolume: data are not loaded yet: {self.filepath}")

        o = (slice(2, None, -1), slice(None, 2, -1)) if order == 'xyz' else (slice(None, 3), slice(3, None))
        return numpy.array([self.metadata['boxes'][i][o[0]] + self.metadata['boxes'][i][o[1]]
                                for i in range(len(self.metadata['ids']))
                                if self.metadata['ids'][i] >= 0])



    def whole_volume(self):
        if not self.loaded:
            raise ValueError(f"LabelledVolume: data are not loaded yet: {self.filepath}")

        for i in range(len(self.metadata['ids'])):
            if self.metadata['ids'][i] == -1:
                return self.metadata['volumes'][i]


    def whole_centroid(self, order = 'xyz'):
        if not self.loaded:
            raise ValueError(f"LabelledVolume: data are not loaded yet: {self.filepath}")

        for i in range(len(self.metadata['ids'])):
            if self.metadata['ids'][i] == -1:
                o = -1 if order == 'xyz' else 1
                return self.metadata['centroids'][i][::o]


    def whole_box(self, order = 'xyz'):
        if not self.loaded:
            raise ValueError(f"LabelledVolume: data are not loaded yet: {self.filepath}")

        for i in range(len(self.metadata['ids'])):
            if self.metadata['ids'][i] == -1:
                o = (slice(2, None, -1), slice(None, 2, -1)) if order == 'xyz' else (slice(None, 3), slice(3, None))
                return self.metadata['boxes'][i][o[0]] + self.metadata['boxes'][i][o[1]]
