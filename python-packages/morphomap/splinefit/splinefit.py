# System
import numpy
import json

# Local
from . import umath
from . import spline

class Fitter:
    def __init__(self, labelled_volume):
        labelled_volume.load()
        self.data    = labelled_volume.data
        self.labels  = numpy.unique(self.data)
        self.results = {}


    def fit(self, count, sampling, rotation = numpy.identity(3)):
        for label in self.labels:
            if label == 0:
                continue

            _ = self.fitone(label, count, sampling, rotation)

        return self.results


    def fitone(self, label, count, sampling, rotation = numpy.identity(3)):
        # `coordinates` is stored in XYZ order
        coordinates = numpy.where(self.data == label)[::-1]
        center      = numpy.asarray(coordinates).mean(axis=1)

        # Rotation
        # The first row of `uR` corresponds to the coordinates of the X axis
        # (1,0,0) after rotation by the inverted rotation matrix. The second
        # row of `uR` is the same, but with the Y axis, and the third row with
        # the Z axis. Therefore, we have: uR = (rotation^-1).T
        # Because the inverse of a rotation matrix is also its transposed matrix,
        # we have: uR = rotation.T.T = rotation
        uR = rotation

        # Identifying the distance to boundary along each axis
        L = numpy.zeros((3, 2))
        searchRange = max(self.data.shape)

        search = numpy.stack((
            umath.find_intersection_dichotomy(self.data, label, center, center + (searchRange * uR[0, :])),
            umath.find_intersection_dichotomy(self.data, label, center, center - (searchRange * uR[0, :])),
            umath.find_intersection_dichotomy(self.data, label, center, center + (searchRange * uR[1, :])),
            umath.find_intersection_dichotomy(self.data, label, center, center - (searchRange * uR[1, :])),
            umath.find_intersection_dichotomy(self.data, label, center, center + (searchRange * uR[2, :])),
            umath.find_intersection_dichotomy(self.data, label, center, center - (searchRange * uR[2, :]))
        ))

        distances = numpy.sqrt(numpy.sum((search - center) ** 2, axis=1))
        L[0, 0] = distances[0]  # Along +X
        L[0, 1] = distances[1]  # Along -X
        L[1, 0] = distances[2]  # Along +Y
        L[1, 1] = distances[3]  # Along -Y
        L[2, 0] = distances[4]  # Along +Z
        L[2, 1] = distances[5]  # Along -Z

        # Identifying parametrization axes
        Lsum     = numpy.sum(L, axis=1)
        small_ax = 0
        mid_ax   = 1
        great_ax = 2

        # dS is set along the largest axis
        dS = uR[great_ax, :]

        # dT[0] is set along the smallest axis and dT[1] is set along the second smallest axis
        dT = numpy.array([uR[small_ax, :], uR[mid_ax, :]])

        # Identifying north pole
        s0 = center + (L[great_ax, 0]) * dS

        expModel = spline.SplineSphere((count, count))
        sample_points, _ = self.__sample_points(
            label, count, count, sampling, sampling, dT, dS, s0, Lsum[great_ax]
        )
        expModel.initializeFromUniformlySampledPoints(
            sample_points, (sampling, sampling)
        )

        mask = numpy.zeros(self.data.shape, dtype=numpy.int8)
        mask[coordinates[::-1]] = 1
        _, rmse = expModel.rmse(mask, (sampling, sampling))

        self.results[label] = FitResult(label, center, L, rotation, count, count,
                                        float(rmse.mean()), int(rmse.max()),
                                        expModel.controlPoints,
                                        expModel.tangentVectors,
                                        expModel)

        return self.results[label]


    def toJSON(self, jsonpath):
        content = {}
        for label in self.results:
            content[str(label)] = self.results[label].todic()

        with open(jsonpath, "w") as fw:
            json.dump(content, fw, indent = 4, separators=(',', ': '))


    def __sample_points(self, label, latitude_count, longitude_count, latitude_sampling, longitude_sampling, dT, dS, s0, Ls):
        """
        Generate a sample of the points at the surface of the object.

        Returns a tuple made of two elements:
            - A nx3 array with the coordinates (XYZ) of the `n` points
            - A nx2 array with the parameters (longitude, latitude) of the `n` points
        """

        longitudemax = ((longitude_count - 1) * longitude_sampling) + 1
        latitudemax  = latitude_count * latitude_sampling

        points = numpy.zeros((latitudemax * longitudemax, 3))
        params = numpy.zeros((latitudemax * longitudemax, 2))

        alphadir = (
            -1 if numpy.any(numpy.abs(numpy.cross(dT[0], dT[1]) - dS) > 1e-8) else 1
        )

        for j in range(0, longitudemax):
            lonp = float(j) / float(longitude_sampling)
            beta = lonp * numpy.pi / (longitude_count - 1)
            c = s0 - (dS * ((Ls / 2.0) - ((Ls / 2.0) * numpy.cos(beta))))

            for i in range(0, latitudemax):
                latp = float(i) / float(latitude_sampling)

                ip = i + latitudemax * j
                if (j == 0) or (j == longitudemax - 1):
                    points[ip] = c
                else:
                    alpha = alphadir * latp * 2.0 * numpy.pi / latitude_count
                    v = numpy.cos(alpha) * dT[0] + numpy.sin(alpha) * dT[1]
                    points[ip] = umath.find_intersection_dichotomy(
                        self.data, label, c, c + (max(self.data.shape) * v)
                    )

                params[ip, 0] = lonp
                params[ip, 1] = latp

        return points, params


class FitResult:

    def __init__(self, label, center, axes, rotation, longitude_count, latitude_count, rmse_mean, rmse_max, control_points, tangent_vectors, model):
        self.label    = label
        self.center   = center
        self.axes     = axes
        self.rotation = rotation
        self.count    = {"longitude": longitude_count, "latitude": latitude_count}
        self.rmse     = {"mean": rmse_mean, "max": rmse_max}
        self.points   = control_points
        self.vectors  = tangent_vectors
        self.model    = model


    def todic(self):
        ret = {}
        ret['center']          = self.center.tolist()
        ret['L']               = self.axes.tolist()
        ret['R']               = self.rotation.tolist()
        ret['longitude_count'] = self.count['longitude']
        ret['latitude_count']  = self.count['latitude']
        ret['rmse_mean']       = self.rmse['mean']
        ret['rmse_max']        = self.rmse['max']
        ret['controlPoints']   = self.points.tolist()
        ret['tangentVectors']  = self.vectors.tolist()

        return ret

