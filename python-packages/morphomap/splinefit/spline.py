import numpy
import scipy.ndimage
import itertools

from . import spline_help

# Convention:
# If point in a 3D array,
# point[0]=x coordinate
# point[1]=y coordinate
# point[2]=z coordinate

class SplineSphere:
    def __init__(self, M):
        if len(M) != 2:
            raise RuntimeError('M must be a doublet.')

        self.splineGenerator = (spline_help.ExponentialSpline(2*(M[0]-1.0),numpy.pi/float(M[0]-1.0)), spline_help.ExponentialSpline(M[1],2.0*numpy.pi/float(M[1])))

        if M[0] >= self.splineGenerator[0].support and M[1] >= self.splineGenerator[1].support:
            self.M = M
        else:
            raise RuntimeError('Each M must be greater or equal than its spline generator support size.')

        self.PIM=(numpy.pi/float(self.M[0]-1.0), numpy.pi/float(self.M[1]))
        self.halfSupport = (self.splineGenerator[0].support / 2.0, self.splineGenerator[1].support / 2.0)

        self.scale = 1.0 / self.splineGenerator[0].firstDerivativeValue(1.0) # Scale by (M[0]-1) if model is normalized
        self.scaleM = (2.0 * (1.0 - numpy.cos(self.PIM[0])) / (numpy.cos(self.PIM[0] / 2) - numpy.cos(3.0 * self.PIM[0] / 2)), 2.0 * (1.0 - numpy.cos(2.0 * self.PIM[1])) / (numpy.cos(self.PIM[1]) - numpy.cos(3.0 * self.PIM[1])))
        self.controlPoints = numpy.zeros((self.M[1] * (self.M[0] - 2) + 2,3))
        self.tangentVectors = numpy.zeros((4,3))

        self.__precomputed = {}

    def wrapIndex(self, t, k, ind):
        val=t-k
        if k < t-self.halfSupport[ind]:
            if (k + self.M[ind] >= t - self.halfSupport[ind]) and (k + self.M[ind] <= t + self.halfSupport[ind]):
                val = t - (k + self.M[ind]);
        elif k > t+self.halfSupport[ind]:
            if (k - self.M[ind] >= t - self.halfSupport[ind]) and (k - self.M[ind] <= t + self.halfSupport[ind]):
                val = t - (k - self.M[ind])

        return val

    def initializeFromPoints(self, samplePoints, sampleParameters):
        # samplePoints is an N x 3 array containing the surface samples to interpolate
        # sampleParameters is an N x 2 array containing the values of s and t corresponding to the samples in samplePoints
        if len(samplePoints)!=len(sampleParameters):
            raise RuntimeError('samplePoints and sampleParameters must be the same length.')

        if len(samplePoints[0])!=3:
            raise RuntimeError('samplePoints must contain triplets.')

        if len(sampleParameters[0])!=2:
            raise RuntimeError('sampleParameters must contain doublets.')

        N=len(samplePoints)
        phi=numpy.zeros((N,self.M[1]*(self.M[0]+2)))
        sigmaX=numpy.zeros((N))
        sigmaY=numpy.zeros((N))
        sigmaZ=numpy.zeros((N))

        for n in range(0, N):
            sigmaX[n] = samplePoints[n,0]
            sigmaY[n] = samplePoints[n,1]
            sigmaZ[n] = samplePoints[n,2]

            t = sampleParameters[n,1]
            s = sampleParameters[n,0]

            for l in range(-1,self.M[0]+1):
                for k in range(0,self.M[1]):
                    kp = k + (self.M[1] * (l + 1))

                    tVal = self.wrapIndex(t, k, 1)
                    if (tVal > -self.halfSupport[1]) and (tVal < self.halfSupport[1]):
                        basisFactor1 = self.splineGenerator[1].value(tVal)
                    else:
                        basisFactor1 = 0

                    sVal = s - l
                    if (sVal > -self.halfSupport[0]) and (sVal < self.halfSupport[0]):
                        basisFactor2 = self.splineGenerator[0].value(sVal)
                    else:
                        basisFactor2 = 0

                    phi[n,kp] = phi[n,kp] + (basisFactor1 * basisFactor2)

        cX=numpy.linalg.lstsq(phi,sigmaX, rcond = None)
        cY=numpy.linalg.lstsq(phi,sigmaY, rcond = None)
        cZ=numpy.linalg.lstsq(phi,sigmaZ, rcond = None)

        splineCoefs=numpy.zeros((self.M[1],self.M[0]+2,3))
        for l in range(-1,self.M[0]+1):
            for k in range(0,self.M[1]):
                kp = k + (self.M[1] * (l + 1))
                splineCoefs[k,l + 1] = numpy.array([cX[0][kp], cY[0][kp], cZ[0][kp]])

        # Non-pole control points
        for l in range(1,self.M[0]-1):
            for k in range(0,self.M[1]):
                self.controlPoints[k + (l - 1) * self.M[1]] = splineCoefs[k][l + 1]

        # Poles and tangent vectors
        cN  = numpy.zeros((3))
        cS  = numpy.zeros((3))
        T1N = numpy.zeros((3))
        T2N = numpy.zeros((3))
        T1S = numpy.zeros((3))
        T2S = numpy.zeros((3))

        for k in range(0,self.M[1]):
            cN = cN + ((self.splineGenerator[0].value(0.0) * splineCoefs[k,1]) + (self.splineGenerator[0].value(1.0) * (splineCoefs[k,0] + splineCoefs[k,2])))
            cS = cS + ((self.splineGenerator[0].value(0.0) * splineCoefs[k,self.M[0]]) + (self.splineGenerator[0].value(1.0) * (splineCoefs[k,self.M[0]-1] + splineCoefs[k,self.M[0]+1])))

            phi=2.0 * self.PIM[1] * k
            phi1=2.0 * self.PIM[1] * ((k + 1) % self.M[1])
            denominator=self.scaleM[1]*((numpy.sin(phi1)*numpy.cos(phi))-(numpy.cos(phi1)*numpy.sin(phi)))
            T1N += (((numpy.sin(phi1)*(splineCoefs[k,0] - splineCoefs[k,2]))-(numpy.sin(phi)*(splineCoefs[(k + 1) % self.M[1],0] - splineCoefs[(k + 1) % self.M[1],2])))/denominator)
            T2N += (((numpy.cos(phi1)*(splineCoefs[k,0] - splineCoefs[k,2]))-(numpy.cos(phi)*(splineCoefs[(k + 1) % self.M[1],0] - splineCoefs[(k + 1) % self.M[1],2])))/(-1.0*denominator))

            T1S += (((numpy.sin(phi1)*(splineCoefs[k,self.M[0]+1] - splineCoefs[k,self.M[0]-1]))-(numpy.sin(phi)*(splineCoefs[(k + 1) % self.M[1],self.M[0]+1] - splineCoefs[(k + 1) % self.M[1],self.M[0]-1])))/denominator)
            T2S += (((numpy.cos(phi1)*(splineCoefs[k,self.M[0]+1] - splineCoefs[k,self.M[0]-1]))-(numpy.cos(phi)*(splineCoefs[(k + 1) % self.M[1],self.M[0]+1] - splineCoefs[(k + 1) % self.M[1],self.M[0]-1])))/(-1.0*denominator))

        # North pole
        self.controlPoints[self.M[1] * (self.M[0] - 2)] = cN / float(self.M[1])
        # South pole
        self.controlPoints[self.M[1] * (self.M[0] - 2) + 1] = cS / float(self.M[1])
        # North tangent plane
        self.tangentVectors[0]=self.controlPoints[self.M[1] * (self.M[0] - 2)]+(T1N * ((self.M[0]-1.0)/(self.scale*float(self.M[1]))))
        self.tangentVectors[1]=self.controlPoints[self.M[1] * (self.M[0] - 2)]+(T2N * ((self.M[0]-1.0)/(self.scale*float(self.M[1]))))
        # South tangent plane
        self.tangentVectors[2]=self.controlPoints[self.M[1] * (self.M[0] - 2) + 1]+(T1S * ((self.M[0]-1.0)/(self.scale*float(self.M[1]))))
        self.tangentVectors[3]=self.controlPoints[self.M[1] * (self.M[0] - 2) + 1]+(T2S * ((self.M[0]-1.0)/(self.scale*float(self.M[1]))))


    def initializeFromUniformlySampledPoints(self, samplePoints, samplingRate):
        # samplePoints is an Mt*samplingRateT*(((Ms-1)*samplingRateS)+1) x 3 array containing the surface samples to interpolate
        # samplingRate is the rate at which each integer interval of s and t is sampled in samplePoints
        if len(samplingRate)!=2:
            raise RuntimeError('samplingRate must be a doublet.')

        sampleParameters=numpy.zeros((self.M[1]*samplingRate[1]*(((self.M[0]-1)*samplingRate[0])+1),2))
        for j in range(0, ((self.M[0]-1)*samplingRate[0])+1):
            for i in range(0,self.M[1]*samplingRate[1]):
                ip=i+(samplingRate[1]*self.M[1])*j
                t = float(i)/float(samplingRate[1])
                s = float(j)/float(samplingRate[0])
                sampleParameters[ip,0]=s
                sampleParameters[ip,1]=t

        self.initializeFromPoints(samplePoints, sampleParameters)


    def centroid(self):
        centroid=numpy.zeros((3))

        for k in range(0,len(self.controlPoints)):
            centroid+=self.controlPoints[k]

        return centroid/len(self.controlPoints)

    def scale(self, scalingFactor):
        centroid=self.centroid()

        for k in range(0,len(self.controlPoints)):
            vectorToCentroid=self.controlPoints[k]-centroid
            self.controlPoints[k]=centroid+scalingFactor*vectorToCentroid

        for t in range(0,len(self.tangentVectors)):
            vectorToCentroid=self.tangentVectors[t]-centroid
            self.tangentVectors[t]=centroid+scalingFactor*vectorToCentroid

        return

    def sample(self, samplingRate):
        if len(samplingRate)!=2:
            raise RuntimeError('samplingRate must be a doublet.')

        samplingT = range(self.M[1] * samplingRate[1])
        samplingS = range((self.M[0] - 1) * samplingRate[0] + 1)
        samplinglist = list(itertools.product(samplingS, samplingT))
        paramlist = [(float(x[0]) / samplingRate[0], float(x[1]) / samplingRate[1]) for x in samplinglist]

        surfacePoints = [self.parametersToWorld(p) for p in paramlist]
        surfacePoints = numpy.asarray(surfacePoints)

        return ((len(samplingT), len(samplingS)), paramlist, surfacePoints)

    def parametersToWorld(self, params, d=(False,False)):
        point = self.__precomputed.get((params[1], params[0], d[1], d[0]), None)
        if point is not None:
            return point

        point = numpy.zeros((3))
        point = self.nonPoleContributions(params[1], params[0], point, d[1], d[0])
        point = self.northPoleContribution(params[1], params[0], point, d[1], d[0])
        point = self.southPoleContribution(params[1], params[0], point, d[1], d[0])

        self.__precomputed[(params[1], params[0], d[1], d[0])] = point
        return point

    def nonPoleContributions(self, t, s, point, dt=False, ds=False):
        for l in range(1,self.M[0] - 1):
            sVal = s - l
            if (sVal > -self.halfSupport[0]) and (sVal < self.halfSupport[0]):
                for k in range(0,self.M[1]):
                    tVal = self.wrapIndex(t, k, 1)
                    if (tVal > -self.halfSupport[1]) and (tVal < self.halfSupport[1]):
                        if ds and dt:
                            point=point+(self.controlPoints[k + (l - 1) * self.M[1]]*(1/self.PIM[0])*(1/(2*self.PIM[1]))*self.splineGenerator[0].firstDerivativeValue(sVal)*self.splineGenerator[1].firstDerivativeValue(tVal))
                        elif ds:
                            point=point+(self.controlPoints[k + (l - 1) * self.M[1]]*(1/self.PIM[0])*self.splineGenerator[0].firstDerivativeValue(sVal)*self.splineGenerator[1].value(tVal))
                        elif dt:
                            point=point+(self.controlPoints[k + (l - 1) * self.M[1]]*self.splineGenerator[0].value(sVal)*(1/(2*self.PIM[1]))*self.splineGenerator[1].firstDerivativeValue(tVal))
                        else:
                            point=point+(self.controlPoints[k + (l - 1) * self.M[1]]*self.splineGenerator[0].value(sVal)*self.splineGenerator[1].value(tVal))
        return point

    def northPoleContribution(self, t, s, point, dt=False, ds=False):
        # North tangent plane
        NorthV1 = self.tangentVectors[0] - self.controlPoints[self.M[1] * (self.M[0] - 2)]
        NorthV2 = self.tangentVectors[1] - self.controlPoints[self.M[1] * (self.M[0] - 2)]

        # l = -1
        sVal = s+1.0
        if (sVal > -self.halfSupport[0]) and (sVal < self.halfSupport[0]):
            for k in range(0,self.M[1]):
                tVal=self.wrapIndex(t, k, 1)
                if (tVal > -self.halfSupport[1]) and (tVal < self.halfSupport[1]):
                    # compute c[k,-1]
                    ckminus1 = self.controlPoints[k] + (1.0/(self.M[0]-1)) * self.scale * self.scaleM[1] * (numpy.cos(2*self.PIM[1]*k) * NorthV1 + numpy.sin(2*self.PIM[1]*k) * NorthV2)

                    if ds and dt:
                        point = point + (ckminus1 * (1/self.PIM[0]) * self.splineGenerator[0].firstDerivativeValue(sVal) * (1/(2*self.PIM[1])) * self.splineGenerator[1].firstDerivativeValue(tVal))
                    elif ds:
                        point = point + (ckminus1 * (1/self.PIM[0]) * self.splineGenerator[0].firstDerivativeValue(sVal) * self.splineGenerator[1].value(tVal))
                    elif dt:
                        point = point + (ckminus1 * self.splineGenerator[0].value(sVal) * (1/(2*self.PIM[1])) * self.splineGenerator[1].firstDerivativeValue(tVal))
                    else:
                        point = point + (ckminus1 * self.splineGenerator[0].value(sVal) * self.splineGenerator[1].value(tVal))

        # l = 0
        sVal=s
        if (sVal > -self.halfSupport[0]) and (sVal < self.halfSupport[0]):
            for k in range(0,self.M[1]):
                tVal=self.wrapIndex(t, k, 1)
                if (tVal > -self.halfSupport[1]) and (tVal < self.halfSupport[1]):
                    # compute c[k,-1]
                    ckminus1 = self.controlPoints[k] + (1.0/(self.M[0]-1)) * self.scale * self.scaleM[1] * (numpy.cos(2*self.PIM[1]*k) * NorthV1 + numpy.sin(2*self.PIM[1]*k) * NorthV2)
                    # compute c[k,0]
                    ck0 = (self.controlPoints[self.M[1] * (self.M[0] - 2)] - self.splineGenerator[0].value(1.0) * (ckminus1 + self.controlPoints[k])) / self.splineGenerator[0].value(0.0)

                    if ds and dt:
                        point = point + (ck0 * (1/self.PIM[0]) * self.splineGenerator[0].firstDerivativeValue(sVal) * (1/(2*self.PIM[1])) * self.splineGenerator[1].firstDerivativeValue(tVal))
                    elif ds:
                        point = point + (ck0 * (1/self.PIM[0]) * self.splineGenerator[0].firstDerivativeValue(sVal) * self.splineGenerator[1].value(tVal))
                    elif dt:
                        point = point + (ck0 * self.splineGenerator[0].value(sVal) * (1/(2*self.PIM[1])) * self.splineGenerator[1].firstDerivativeValue(tVal))
                    else:
                        point = point + (ck0 * self.splineGenerator[0].value(sVal) * self.splineGenerator[1].value(tVal))

        return point

    def southPoleContribution(self, t, s, point, dt=False, ds=False):
        # South tangent plane
        SouthV1 = self.tangentVectors[2] - self.controlPoints[self.M[1] * (self.M[0] - 2) + 1]
        SouthV2 = self.tangentVectors[3] - self.controlPoints[self.M[1] * (self.M[0] - 2) + 1]

        # l = M+1
        sVal=s-(self.M[0]-1)-1
        if (sVal > -self.halfSupport[0]) and (sVal < self.halfSupport[0]):
            for k in range(0,self.M[1]):
                tVal=self.wrapIndex(t, k, 1)
                if (tVal > -self.halfSupport[1]) and (tVal < self.halfSupport[1]):
                    # compute c[k,M+1]
                    ckMplus = self.controlPoints[k + (self.M[0] - 3) * self.M[1]] + (1.0/(self.M[0]-1)) * self.scale * self.scaleM[1] * (numpy.cos(2*self.PIM[1]*k) * SouthV1 + numpy.sin(2*self.PIM[1]*k) * SouthV2)

                    if ds and dt:
                        point = point + (ckMplus * (1/self.PIM[0]) * self.splineGenerator[0].firstDerivativeValue(sVal) * (1/(2*self.PIM[1])) * self.splineGenerator[1].firstDerivativeValue(tVal))
                    elif ds:
                        point = point + (ckMplus * (1/self.PIM[0]) * self.splineGenerator[0].firstDerivativeValue(sVal) * self.splineGenerator[1].value(tVal))
                    elif dt:
                        point = point + (ckMplus * self.splineGenerator[0].value(sVal) * (1/(2*self.PIM[1])) * self.splineGenerator[1].firstDerivativeValue(tVal))
                    else:
                        point = point + (ckMplus * self.splineGenerator[0].value(sVal) * self.splineGenerator[1].value(tVal))

        # l = M
        sVal=s-(self.M[0]-1)
        if (sVal > -self.halfSupport[0]) and (sVal < self.halfSupport[0]):
            for k in range(0,self.M[1]):
                tVal=self.wrapIndex(t, k, 1)
                if (tVal > -self.halfSupport[1]) and (tVal < self.halfSupport[1]):
                    # compute c[k,M+1]
                    ckMplus = self.controlPoints[k + (self.M[0] - 3) * self.M[1]] + (1.0/(self.M[0]-1)) * self.scale * self.scaleM[1] * (numpy.cos(2 * self.PIM[1] * k) * SouthV1 + numpy.sin(2 * self.PIM[1] * k) * SouthV2)
                    # compute c[k,M]
                    ckM = (self.controlPoints[self.M[1] * (self.M[0] - 2) + 1] - self.splineGenerator[0].value(1.0) * (ckMplus + self.controlPoints[k + (self.M[0] - 3) * self.M[1]])) / self.splineGenerator[0].value(0.0)

                    if ds and dt:
                        point = point + (ckM * (1/self.PIM[0]) * self.splineGenerator[0].firstDerivativeValue(sVal) * (1/(2*self.PIM[1])) * self.splineGenerator[1].firstDerivativeValue(tVal))
                    elif ds:
                        point = point + (ckM * (1/self.PIM[0]) * self.splineGenerator[0].firstDerivativeValue(sVal) * self.splineGenerator[1].value(tVal))
                    elif dt:
                        point = point + (ckM * self.splineGenerator[0].value(sVal) * (1/(2*self.PIM[1])) * self.splineGenerator[1].firstDerivativeValue(tVal))
                    else:
                        point = point + (ckM * self.splineGenerator[0].value(sVal) * self.splineGenerator[1].value(tVal))

        return point

    def rmse(self, mask, samplingRate):
        """
        Compute the RMSE using the given mask as a reference.
        """
        if len(samplingRate)!=2:
            raise RuntimeError('samplingRate must be a doublet.')

        grad    = numpy.gradient(mask)
        gradMag = grad[0] ** 2 + grad[1] ** 2 + grad[2] ** 2
        surface = self.sample(samplingRate)
        points  = numpy.floor(surface[2]).astype(int)

        numpy.unique(points.round().astype(int), axis = 0)

        gradMag[gradMag > 0] = 1
        edm  = scipy.ndimage.distance_transform_cdt(1 - gradMag)

        maxedm = mask.shape[0] ** 2 + mask.shape[1] ** 2 + mask.shape[2] ** 2
        res    = numpy.asarray([maxedm] * points.shape[0])
        validp = numpy.where((
            (points[:, 0] >= 0).astype(int) +
            (points[:, 0] < mask.shape[2]).astype(int) +
            (points[:, 1] >= 0).astype(int) +
            (points[:, 1] < mask.shape[1]).astype(int) +
            (points[:, 2] >= 0).astype(int) +
            (points[:, 2] < mask.shape[0]).astype(int)) == 6)

        res[validp[0]] = edm[tuple(points[validp[0], ::-1].T)] ** 2

        return surface[1], res

