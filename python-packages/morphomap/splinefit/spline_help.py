import numpy

class SplineGenerator:
    unimplementedMessage = "This function is not implemented."

    def __init__(self, multigenerator, support):
        self.multigenerator = multigenerator
        self.support = support

    def value(self, t):
        # This needs to be overloaded
        raise NotImplementedError(SplineGenerator.unimplementedMessage)

    def firstDerivativeValue(self, t):
        # This needs to be overloaded
        raise NotImplementedError(SplineGenerator.unimplementedMessage)


class ExponentialSpline(SplineGenerator):
    def __init__(self, M, alpha):
        SplineGenerator.__init__(self, False, 3.0)
        self.alpha = alpha
        self.M = M

    def value(self, t):
        t += self.support / 2.0
        L = (numpy.sin(numpy.pi / self.M) / (numpy.pi / self.M)) ** (-2)

        val = 0.0
        if t >= 0 and t < 1:
            val = 2.0 * numpy.sin(self.alpha * 0.5 * t) * numpy.sin(self.alpha * 0.5 * t)
        elif t >= 1 and t < 2:
            val = (numpy.cos(self.alpha * (t - 2)) +
                   numpy.cos(self.alpha * (t - 1)) -
                   numpy.cos(self.alpha) * 2.0)
        elif t >= 2 and t <= 3:
            val = (
                2.0
                * numpy.sin(self.alpha * 0.5 * (t - 3))
                * numpy.sin(self.alpha * 0.5 * (t - 3))
            )

        return (L * val) / (self.alpha * self.alpha)

    def firstDerivativeValue(self, t):
        t += self.support / 2.0
        L = (numpy.sin(numpy.pi / self.M) / (numpy.pi / self.M)) ** (-2)

        val = 0.0
        if 0 <= t and t <= 1:
            val = self.alpha * numpy.sin(self.alpha * t)
        elif 1 < t and t <= 2:
            val = self.alpha * (
                numpy.sin(self.alpha * (1 - t)) + numpy.sin(self.alpha * (2 - t))
            )
        elif 2 < t and t <= 3:
            val = self.alpha * numpy.sin(self.alpha * (t - 3))

        return (L * val) / (self.alpha * self.alpha)
