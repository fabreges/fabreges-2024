import numpy

def find_intersection_dichotomy(volume, label, p0, p1):
    """
    Try to find the coordinates in between `p0` and `p1` where
    the label is changing from `label` to something else. If the
    label value in `p0` does not equal the one provided in
    the argument list, then the `p0` position is returned.

    The value of the label is checked in `pa` and in `p` with
    `p=(pa+pb)/2`. The algorithm is initialised with `pa=p0`
    and `pb=p1`. If the pixel value in `p` equals the value in `pa`,
    then we continue with `pa` set to `p`. If the poixel value in
    `p` equals the value in `pb`, then we continue with `pb` set to `p`.
    The algorithm stops when the distance between `pa` and `pb` is
    less than half a pixel.

    The order of the coordinates must be X, Y and Z.
    """

    i0 = numpy.asarray(p0)
    i1 = numpy.asarray(p1)

    if numpy.sum((i0 - i1) ** 2) < 0.25:
        return i0

    if numpy.any(i1 < 0) or numpy.any(i1 >= volume.shape[::-1]):
        i1 = find_edge_dichotomy(volume, i0, i1)

    label0 = volume[tuple(i0.astype(int)[::-1])]
    if label0 != label:
        return i0

    # The code below will look for the label in between i0 and i1.
    # By default, we use dichotomic search (ratio = 0.5), but we may
    # end up outside of the volume boundaries. If this is the case,
    # we get closer to i0 by decreasing the value of `ratio` until we
    # find a valid label
    labelm = -1
    ratio  = 0.5
    while labelm == -1 and ratio > 0:
        im = numpy.asarray((i0 + i1) * ratio)
        try:
            labelm = volume[tuple(im.astype(int)[::-1])]
        except IndexError:
            ratio = ratio - 0.01
            labelm = -1

    if labelm == label0:
        return find_intersection_dichotomy(volume, label, im, i1)
    else:
        return find_intersection_dichotomy(volume, label, i0, im)


def find_edge_dichotomy(volume, i0, i1):
    """
    Look for the edge of the volume in between `i0` and `i1`.
    A much faster implementation is possible (but this is not
    the bottleneck of the pipeline).
    """

    inf  = 0
    sup  = 1
    step = 1.0 / max(volume.shape)
    while (sup - inf) > step:
        factor = (inf + sup) / 2.0
        ii = i0 + factor * (i1 - i0)

        if numpy.logical_or(ii < 0, ii >= volume.shape[::-1]).any():
            sup = factor
        else:
            inf = factor

    ii = i0 + inf * (i1 - i0)
    return ii
