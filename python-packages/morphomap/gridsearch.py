import numpy
import logging
logger = logging.getLogger(__name__)

class GridSearch:

    def __init__(self, numparam = 1):
        self.numparam   = numparam
        self.params     = [[None] * numparam]
        self.fun_cost   = self.__default_fun_cost
        self.fun_values = self.__default_fun_values
        self.stop       = False
        self.iterations = 0


    def search(self, keep = 3, alpha = 0, alphaR = 1, epsilon = 1e-6, patience = 10, levelmax = 0, target_cost = 0, vectorized_cost = False):
        """
        Search the best solution using the grid search algorithm.
        Parameters:
          - keep   :int(1,)       - maximum number of `best solutions` to keep between two iterations
          - alpha  :float(0,1)    - probability of keeping a value that is not better than the current best.
          - alphaR :float(0,1)    - multiplicative factor at each iteration of the `alpha` parameter.
          - epsilon:float(0, )    - stop condition, the search stops when the improvement is lower than epsilon. Use `0` to disable.
          - patience:int(2, )     - stop rule, specify the number of iterations to use to compute the `epsilon` stop conditions.
          - levelmax:int(0, )     - stop condition, the search stops when the given level is reached. Use `0` to disable.
          - target_cost:float(0,) - stop condition, the search stops when the best cost is lower than the given one. Use `0` to disable.
          - vectorized_cost:bool  - whether the cost function can handle all the combination at once or not.

        Gradient Descent:    use alpha = 0 and alphaR = 1
        Simulated annealing: use alpha > 0 and alphaR < 1
        Multibest search:    use alpha = 1 and alphaR = 1
        """

        patience = max(2, patience)
        level    = 1
        bestC    = None
        bestV    = None
        errors   = []
        while True:
            values = numpy.empty((0, self.numparam))
            for c in range(len(self.params)):
                p = []
                for i in range(self.numparam):
                    p += [self.fun_values(i, self.params[c][i], level)]

                mesh   = numpy.asarray([m.flatten() for m in self.__meshgrid(*p, sparse = False)]).T
                values = numpy.vstack((values, mesh))

            values = numpy.unique(values, axis = 0)

            if vectorized_cost:
                self.iterations += 1
                costs = self.fun_cost(*(values.T))
            else:
                costs = []
                for i in range(len(values)):
                    self.iterations += 1
                    costs += [self.fun_cost(*values[i,:])]

            costs = numpy.asarray(costs)

            if bestC is None:
                bestC = max(costs)

            annealing = numpy.logical_or(costs <= bestC, numpy.random.random(len(costs)) < alpha)
            if numpy.all(~annealing):
                return bestV, bestC

            values  = values[annealing, ]
            costs   = costs[annealing, ]

            sortind = numpy.argsort(costs)
            costs   = costs[sortind][:keep]
            values  = values[sortind, :][:keep]

            bestC   = costs[0]
            bestV   = values[0,:]

            errors += [bestC]
            if len(errors) >= patience and numpy.std(errors[-patience:]) < epsilon:
                return bestV, bestC

            if level == levelmax:
                return bestV, bestC

            if bestC < target_cost:
                return bestV, bestC

            self.params = numpy.array(values, copy = True)
            alpha      *= alphaR
            level      += 1

            if self.stop:
                return bestV, bestC


    def __default_fun_cost(self, *params):
        logger.error("This is the default method to compute the cost of a parameter combination.")
        logger.error("This method does nothing but raises an error. This method *must* be implemented")
        logger.error("by the user. It takes a list of parameter values as argument and returns a")
        logger.error("single positive floating number. The grid search will try to minimise the cost.")
        logger.error("Exemple:")
        logger.error("def cost(*params):")
        logger.error("  return sum(params)")
        logger.error("")

        raise NotImplementedError("The method `fun_cost` of the `GridSearch` instance must be user-defined.")


    def __default_fun_values(self, index, center, level):
        logger.error("This is the default method to compute the values to explore. This method does")
        logger.error("nothing but raises an error. This method *must* be implemented by the user. It")
        logger.error("takes three parameters and returns a list of values.")
        logger.error("Parameters:")
        logger.error("   index:int  – the index of the parameter, starting at zero. With `n` parameters,")
        logger.error("                this method will be called `n` times.")
        logger.error("   center:int - the middle value of the interval, or `None` for the first round.")
        logger.error("   level:int  - the depth of refinement (starts at 1). The first grid is generated")
        logger.error("                by calling this methods with level=1. The second grid by level=2, etc.")
        logger.error("")
        logger.error("Exemple:")
        logger.error("def values(index, center, level):")
        logger.error("  center = 180 if center is None else center")
        logger.error("  size   = round(180 / level)")
        logger.error("  return [center - size + 2 * size * x / 100 for x in range(101)]")
        logger.error("")

        raise NotImplementedError("The method `fun_values` of the `GridSearch` instance must be user-defined.")


    def __meshgrid(self, *xi, copy = True, sparse = True, indexing = 'xy'):
        if numpy.meshgrid:
            return numpy.meshgrid(*xi, copy = copy, sparse = sparse, indexing = indexing)
        else:
            ndim = len(xi)

            if indexing not in ['xy', 'ij']:
                raise ValueError(
                    "Valid values for `indexing` are 'xy' and 'ij'.")

            s0 = (1,) * ndim
            output = [numpy.asanyarray(x).reshape(s0[:i] + (-1,) + s0[i + 1:])
                for i, x in enumerate(xi)]

            if indexing == 'xy' and ndim > 1:
                # switch first and second axis
                output[0].shape = (1, -1) + s0[2:]
                output[1].shape = (-1, 1) + s0[2:]

            if not sparse:
                # Return the full N-D matrix (not only the 1-D vector)
                output = numpy.broadcast_arrays(*output, subok=True)

            if copy:
                output = [x.copy() for x in output]

            return output
