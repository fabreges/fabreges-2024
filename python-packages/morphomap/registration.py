from .gridsearch import GridSearch

import numpy

class Registration:

    def __init__(self, v1, v2):
        """
        Computes transformations that minimise the distance
        between the points given in `v1` and `v2`. The correspondance
        is found using a nearest neighbours algorithm.

        The results are stored in `self.translation` and `self.rotation`
        for the translation and the rotation matrix respectively.
        """

        v1.load()
        v2.load()

        self.seeds1 = v1.centroids(order = 'xyz') - v1.whole_centroid(order = 'xyz')
        self.ids1   = v1.ids()

        self.seeds2 = v2.centroids(order = 'xyz') - v2.whole_centroid(order = 'xyz')
        self.ids2   = v2.ids()

        count1 = len(self.seeds1)
        count2 = len(self.seeds2)
        if count1 != count2:
            raise ValueError(f"Unable to register datasets with different number of points ({count1} and {count2}).")

        self.numpoints = count1
        self.bestcost  = float('Inf')
        self.links     = None

        self.gs            = GridSearch(3)
        self.gs.fun_values = self.fun_values
        self.gs.fun_cost   = self.fun_cost
        res                = self.gs.search(patience = 2, levelmax = 10)

        self.rotation    = Registration.rotmat(*res[0], rad = False)
        self.translation = numpy.zeros((1,3))
        self.rawreg      = res


    def fun_values(self, index, center, level):
        initial_step = 10
        window       = 5
        subwin       = window - 1

        if level == 1:
            return range(0, 361, initial_step)

        return [center - initial_step * (subwin - 2 * i) / (2 * subwin ** (level - 1)) for i in range(window)]


    def fun_cost(self, alpha, beta, gamma):
        rotmat = Registration.rotmat(alpha, beta, gamma, rad = False)

        distances   = []
        transformed = []
        for ic in range(self.numpoints):
            obj  = self.seeds2[ic]
            tobj = obj @ rotmat.T
            diff = numpy.sum((self.seeds1 - tobj) ** 2, axis = 1)

            transformed += [tobj]
            distances   += [(self.ids1[xi], self.ids2[ic], xv) for xi, xv in enumerate(diff)]

        distances.sort(key = lambda x: x[2])
        from_obj2 = []
        from_obj1 = []
        cost      = 0
        count     = 0
        for d in distances:
            if d[0] in from_obj1 or d[1] in from_obj2:
                continue

            from_obj1 += [d[0]]
            from_obj2 += [d[1]]

            cost  += numpy.sqrt(d[2])
            count += 1
            if cost > self.bestcost:
                break

            if count == self.numpoints:
                break

        if cost < self.bestcost:
            self.links    = list(zip(from_obj1, from_obj2))
            self.bestcost = cost

        return cost


    @staticmethod
    def rotmat(alpha, beta, gamma, rad = False):
        """
        Build a rotation matrix
        """
        rotations = numpy.array([alpha, beta, gamma])

        if not rad:
            rotations *= numpy.pi / 180

        cosa, cosb, cosg = numpy.cos(rotations)
        sina, sinb, sing = numpy.sin(rotations)
        return numpy.array([[cosb*cosg, sina*sinb*cosg-cosa*sing, cosa*sinb*cosg+sina*sing],
                            [cosb*sing, sina*sinb*sing+cosa*cosg, cosa*sinb*sing-sina*cosg],
                            [-sinb,     sina*cosb,                cosa*cosb,              ]])

