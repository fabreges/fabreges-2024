#! /usr/bin/env python
import argparse

import morphomap
from shared import logger

parser = argparse.ArgumentParser(description = "Measure the volume of cells and objects.")
parser.add_argument("-a", "--append",     action = "store_true", default = False,                                              help = "Whether to append the result to the output file, or to overwrite the destination file (default).")
parser.add_argument("-r", "--resolution", type = float,          nargs = 3, metavar = ("dx", "dy", "dz"), default = (1, 1, 1), help = "Specify the physical size of the voxels.")
parser.add_argument("input",              type = str,                                                                          help = "Input tif or tif.gz file representing labelled cells.")
parser.add_argument("output",             type = str,                                                                          help = "CSV file where data must be stored.")
args = parser.parse_args()

logger.info("-- VOLUME MEASUREMENT --")
logger.info(f"Spatial resolution: {args.resolution}")
logger.info(f"Input data:         {args.input}")
logger.info(f"Output file:        {args.output}")

volume = morphomap.LabelledVolume(args.input)
volume.load()

cell_volumes   = volume.volumes()
cell_centroids = volume.centroids()
cell_ids       = volume.ids()
cell_count     = volume.count()
voxel_volume   = args.resolution[0] * args.resolution[1] * args.resolution[2]

if not args.append:
    with open(args.output, "w") as fw:
        fw.write("obj_id,volume,x,y,z\n")

with open(args.output, "a") as fa:
    for i in range(cell_count):
        fa.write(f"{cell_ids[i]},{cell_volumes[i] * voxel_volume},{cell_centroids[i,0] * args.resolution[0]},{cell_centroids[i,1] * args.resolution[1]},{cell_centroids[i,2] * args.resolution[2]}\n")

