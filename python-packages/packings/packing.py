import numpy

class Packing():

    def __init__(self, matrix):
        """
        Generate a packing based on adjacency matrix for 8 objects
        """
        self.matrix      = self.__symmetrize(numpy.asarray(matrix))
        self.matrix_bin  = self.__binarise(self.matrix)
        self.matrix_norm = self.__normalise(self.matrix, 36)


    def rigidity_threshold(self, normalised = True):
        """
        Finds the normalised contact threshold so that as many small
        contacts as possible are removed, but rigidity is preserved.
        """
        if not self.is_rigid(self.matrix_bin):
            raise ValueError("Matrix is not rigid")

        breaks    = numpy.unique(self.matrix_norm)
        threshold = 0
        for ti in range(len(breaks)):
            pruned_norm = self.matrix_norm.copy()
            pruned_norm[pruned_norm <= breaks[ti]] = 0

            pruned_bin = self.__binarise(pruned_norm)
            if not self.is_rigid(pruned_bin):
                break

            threshold = ti

        if normalised:
            return breaks[threshold]

        return numpy.unique(self.matrix)[threshold]


    def distance(self, ref_matrix, threshold = None):
        """
        Find distance to a reference packing
        """

        if threshold is None:
            threshold = self.rigidity_threshold()

        pruned_norm = self.matrix_norm.copy()
        pruned_norm[pruned_norm <= threshold] = 0
        pruned_bin = self.__binarise(pruned_norm)
        wA, _ = numpy.linalg.eig(pruned_bin)

        ref_bin = self.__binarise(numpy.asarray(ref_matrix))
        wB, _ = numpy.linalg.eig(ref_bin)

        wA.sort()
        wB.sort()

        return numpy.sqrt(((wA - wB) ** 2).sum())


    def is_rigid(self, matrix):
        """
        Test rigidity of a binary adjacency matrix
        """

        if not self.is_binary(matrix):
            raise ValueError("Adjacency matrix must be binary to test for rigidity")

        rules = {4: {"total": 12, "each": 3}, 8: {"total": 36, "each": 3}}
        size  = len(matrix)
        if matrix.sum() < rules[size]['total']:
            return False

        if (matrix.sum(axis = 0) < rules[size]['each']).any():
            return False

        return True


    def is_binary(self, matrix):
        """
        Tests whether a matrix is binarised
        """
        return numpy.in1d((0,1), matrix.flat).all()


    def __symmetrize(self, matrix, method = numpy.mean):
        """
        Makes the adjacency matrix symmetrical.
        `method` must returns one real number and
        receives a tuple with the values from the
        upper and the lower triangle respectively.
        """
        m = matrix.copy()
        for i in range(1, m.shape[0]):
            for j in range(i):
                v = method((m[j,i], m[i,j]))
                m[(i,j),(j,i)] = v

        return m

    def __binarise(self, matrix):
        """
        Binarization of the adjacency matrix
        """
        m = matrix.copy()
        m[m > 0] = 1
        m[m < 1] = 0

        return m


    def __normalise(self, matrix, s):
        """
        Normalisation of the adjacency matrix values
        """
        m = matrix.copy()
        if len(m.shape) != 2 or m.shape[0] != m.shape[1]:
            raise ValueError("Adjacency matrix must be square")

        for i in range(m.shape[0]):
            m[i, i] = 0

        return m * s / m.sum()
