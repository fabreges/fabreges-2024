###################################################################################
###### Coordinates center of masses spheres of rigid packings from:
###### Natalie Arkus, Vinothan N. Manoharan, and Michael P. Brenner
###### Deriving Finite Sphere Packings
###### SIAM Journal on Discrete Mathematics. 25 (4) (2011) doi: 10.1137/100784424
###################################################################################

Td = {"name": "td",
      "display_name": "Td",
      "packing": [[0.0,                0.0,                0.0],
                  [0.0,                -1.0,               0.0],
                  [0.8660254038,       -0.5,               0.0],
                  [0.2886751346,       -0.5,               0.8164965809]],
        "adj_matrix": [[0, 1, 1, 1],
                       [1, 0, 1, 1],
                       [1, 1, 0, 1],
                       [1, 1, 1, 0]]}

