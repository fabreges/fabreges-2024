from .packing import Packing
from .seed import Seed
from . import rbb8
from . import rbb4

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter(
                fmt     = "[{asctime:s}]: {name}: {levelname:s}: {message:s}",
                datefmt = "%y-%m-%d %H:%M:%S",
                style   = "{")

handler = logging.StreamHandler()
handler.setLevel(logging.DEBUG)
handler.setFormatter(formatter)

logger.addHandler(handler)

