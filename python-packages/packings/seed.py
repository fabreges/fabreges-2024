"""
Created on Sat Sep 30 14:25:12 2023

@author: bernat
"""

import numpy
import scipy.spatial
import random

###################################################################################
#############  Create_3D_packing_seed(..,..,..,..)
#############  Creates the seed for the energy optimization using surface evolver software
#############  creates collection of simplexes consistently oriented
#############  input: center of mass of the spheres, network of contact among spheres
#############  radius of the spheres, volume of the spheres
#############  output: .fe code to be read by the surface evolver software
###################################################################################

class Seed:

    def __init__(self, centroids, graph, volumes):
        centroids = numpy.asarray(centroids)

        nodes     = []
        count     = centroids.shape[0]
        vertices  = [[] for x in range(count)]

        neighbours = []
        for i in range(count):
            nb = set()

            for k in range(len(graph)):
                if graph[k][0] == i:
                    nb.add(graph[k][1])

                if graph[k][1] == i:
                    nb.add(graph[k][0])

            neighbours.append(list(nb))

        matrix = numpy.zeros((count, count))
        radii  = [None] * count
        for i in range(count):
            nb = neighbours[i]
            rmin = float('Inf')

            for k in range(len(nb)):
                u = nb[k]
                d = self.__distance(centroids[i,], centroids[u,])
                if d < rmin:
                    rmin = d

                matrix[i, u] = 1

            radii[i] = rmin / 2

        ###########################################################
        #       Creating the cloud points and contacts
        ###########################################################
        for i in range(count):
            r = radii[i]
            for u in range(150):
                alpha = random.uniform(0, numpy.pi)
                beta  = random.uniform(0, 2*numpy.pi)
                v     = self.__radial_point(centroids[i,], r, alpha, beta)

                vertices[i].append(v)
                nodes.append(v)

            for k in range(i+1, count):
                if matrix[i, k] == 1:
                    mp = self.__centroid(centroids[(i,k),])

                    vertices[i].append(mp)
                    vertices[k].append(mp)
                    nodes.append(mp)

                    tri = self.__triangle_in_plane(centroids[i,], centroids[k,], mp, 10000, 0.2)
                    vertices[i].append(tri[0])
                    vertices[k].append(tri[0])
                    vertices[i].append(tri[1])
                    vertices[k].append(tri[1])
                    vertices[i].append(tri[2])
                    vertices[k].append(tri[2])

                    nodes.append(tri[0])
                    nodes.append(tri[1])
                    nodes.append(tri[2])

        ###########################################################
        # Strategy: finding the convex hulls of the set of points (contacts, center of mass)
        ###########################################################
        hulls = [] # list with the convex hulls
        for i in range(count):
            hull      = []
            points    = numpy.asarray(vertices[i])
            hcentroid = self.__centroid(points)

            hull      = scipy.spatial.ConvexHull(points)

            _hull = []
            for s in hull.simplices:
                s = numpy.append(s, s[0])  # Here we cycle back to the first coordinate
                _hull.append(s.tolist())

            hulls.append({"hull": _hull, "centroid": hcentroid})

        # Renumbering the contact points such that the
        # same point has always the same ID/contact
        for i in range(count):
            hull = hulls[i]['hull']
            for k in range(len(hull)):
                for u in range(len(hull[k])):
                    index = hull[k][u]
                    for j in range(len(nodes)):
                        if (vertices[i][index] == nodes[j]).all():
                            hull[k][u] = j

        # Identifying links
        links = []
        for i in range(count): 
            hull = hulls[i]['hull']

            for k in range(len(hull)):
                l0 = hull[k][0]
                l1 = hull[k][1]
                l2 = hull[k][2]
                l3 = hull[k][3]

                links.append((l0, l1) if l0 <= l1 else (l1, l0))
                links.append((l1, l2) if l1 <= l2 else (l2, l1))
                links.append((l2, l3) if l2 <= l3 else (l3, l2))

        links = numpy.unique(links, axis=0)

        ################################################################
        # Identifying the direction the link is traversed in the Convex Hull
        # a vertex is traversed u=[i,j], -u=[j,i]
        ################################################################

        paths = []
        for i in range(0, count):
            hull = hulls[i]['hull']
            paths.append([])

            for k in range(len(hull)):
                paths[i].append([])

                for l in range(len(hull[k]) - 1):
                    n0 = hull[k][l]
                    n1 = hull[k][l+1]

                    for u in range(0, len(links)):
                        if links[u, 0] == n0 and links[u, 1] == n1:
                            paths[i][k].append(u+1)
                            break

                        if links[u, 0] == n1 and links[u, 1] == n0:
                            paths[i][k].append(-u-1)
                            break

        ###########################################################
        # Orienting the facets
        ###########################################################

        orientation = []
        for w in range(count):
            hull_coord = []
            hull       = hulls[w]['hull']
            hcentroid  = hulls[w]['centroid']

            for u in range(len(hull)):
                hull_coord.append([])
                for k in range(0, len(hull[u])):
                    hull_coord[u].append(nodes[hull[u][k]])

            hull_coord   = numpy.array(hull_coord)
            orientation.append([])
            for i in range(len(hull_coord)):
                v1 = hull_coord[i, 1] - hull_coord[i, 0]
                v2 = hull_coord[i, 0] - hull_coord[i, 2]
                vc = hull_coord[i, 0] - hcentroid

                rotational  = numpy.cross(v1, v2)
                scalar_prod = numpy.dot(rotational,vc)

                if scalar_prod < 0:
                    orientation[-1].append(1)
                else:
                    orientation[-1].append(-1)

        ############## Identifying the simplices
        facets = []
        for u in range(count):
            hull = hulls[u]['hull']
            facets.append([])

            for i in range(len(hull)):
                facets[u].append([])
                for k in range(1, len(hull[i])):
                    v1=hull[i][k-1]
                    v2=hull[i][k]

                    for j in range(len(links)):
                        if links[j, 0] == v1 and links[j, 1] == v2:
                            facets[u][i].append(j+1)
                            break

                        if links[j, 0] == v2 and links[j, 1] == v1:
                            facets[u][i].append(-j-1)
                            break

        data = (nodes, links, paths, facets)

        self.nodes  = nodes
        self.links  = links
        self.paths  = paths
        self.facets = facets
        self.data   = data

        self.orientation = orientation
        self.volumes     = volumes


    def save_fe(self, filepath):
        """
        Save as Surface Evolver file
        """

        with open(filepath, 'w') as fw:
            fw.write('// Evolver data for one bubble of prescribed position and volume.')
            fw.write('\n')
            fw.write('vertices\n')
            fw.write('\n')

            ####################### Print vertices
            for i in range(len(self.nodes)):
                node = self.nodes[i]
                fw.write(f"{i+1}  {node[0]} {node[1]} {node[2]}\n")

            fw.write('\n')

            ####################### Print edges
            fw.write('edges  /* given by endpoints */\n')
            for i in range(len(self.links)):
                link = self.links[i]
                fw.write(f"{i+1}  {link[0]+1} {link[1]+1}\n")

            fw.write('\n')

            ####################### Print faces
            fw.write('faces  /* given by oriented edge loop */\n')

            n = 1
            for i in range(len(self.paths)):
                path = self.paths[i]
                for pp in path:

                    fw.write(f"{n}  {pp[0]} {pp[1]} {pp[2]}\n")
                    n += 1

            ##### printing bodies
            fw.write('\n\n')
            fw.write('bodies  /* defined by their oriented faces */\n')

            n = 1
            for i in range(len(self.facets)):
                facet = self.facets[i]
                fw.write(f"{i+1}    ")
                for k in range(len(facet)):
                    fw.write(f"{self.orientation[i][k] * n}  ")
                    n += 1

                fw.write(f"volume {4.5 * self.volumes[i]}\n")

            fw.write('\n')
            fw.write('read')

    def __centroid(self, pts):
        return pts.mean(axis = 0)

    def __distance(self, a, b):
        return numpy.sqrt(numpy.sum((a - b) ** 2))

    def __radial_point(self, u, r, alpha, beta): # r>0, alpha in (0, pi), beta in (0, 2pi)
        w1 = r * numpy.sin(alpha) * numpy.cos(beta)
        w2 = r * numpy.sin(alpha) * numpy.sin(beta)
        w3 = r * numpy.cos(alpha)
        w  = u + (w1, w2, w3)

        return(w)

    def __triangle_in_plane(self, p1, p2, p3, n, r):
        cloud = []
        for i in range(n):
            v=[]
            alpha = random.uniform(0, numpy.pi)
            beta  = random.uniform(0, 2*numpy.pi)
            v     = self.__radial_point(p3, r, alpha, beta)
            cloud.append(v)

        dist = []
        for i in range(n):
            rel = numpy.abs(self.__distance(p1, cloud[i]) - self.__distance(p2, cloud[i]))
            dist.append([i, rel])

        dist.sort(key = lambda x: (x[1]))
        u1 = dist[0][0]
        u2 = dist[1][0]
        u3 = dist[2][0]
        u4 = dist[3][0]

        return (cloud[u1], cloud[u2], cloud[u3], cloud[u4])
