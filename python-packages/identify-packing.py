#! /usr/bin/env python
import argparse
import sys

import packings
from shared import logger

parser = argparse.ArgumentParser(description = "Identify the rigidity status or the closest rigid packing at 4- or 8-cell stage.")
parser.add_argument("-H", "--no-header", action = "store_true",   default = False, help = "If not set, the command will output the CSV's header line too.")
parser.add_argument("matrix",            type = str,                               help = "Adjacency matrix as output by `estimate-contacts.py`. The matrix is a file with coma-separated columns and newline-separated rows. The first column must be an identifier of the object, or `0` for the background (the background will be ignored when identifying packings). The second and third columns are not used. The n-th column (with n≥4) MUST correspond to the same object as the (n-2)th row (counting the header row). The diagonal (starting at column 4 row 2) should be filled with non-numeric data (typically: NA).")
parser.add_argument("output",            type = str, nargs = "?", default = None,  help = "CSV file where data must be stored, or stdout if not specified.")
args = parser.parse_args()

logger.info("-- IDENTIFY PACKING --")
logger.info(f"No header:   {args.no_header}")
logger.info(f"Matrix:      {args.matrix}")
logger.info(f"Output file: {args.output}")

background = None
matrix     = []
with open(args.matrix, "r") as fr:
    lineno = 0
    for line in fr:
        lineno += 1
        if lineno == 1:
            continue

        elements = line.strip().split(",")
        obj_id   = int(elements[0])
        if obj_id == 0:
            background = lineno-2
            continue

        contacts = []
        for x in elements[3:]:
            try:
                contacts += [float(x)]
            except ValueError:
                contacts += [0]

        matrix += [contacts]


if background is not None:
    for i in range(len(matrix)):
        del matrix[i][background]

size = len(matrix)
if size == 4:
    all_rbb = (packings.rbb4.Td, )
elif size == 8:
    all_rbb = (packings.rbb8.Td,   packings.rbb8.Cs_1, packings.rbb8.Cs_2,  packings.rbb8.Cs_3,
            packings.rbb8.D2d,  packings.rbb8.C1_1, packings.rbb8.C2v_1, packings.rbb8.C2v_2,
            packings.rbb8.C1_2, packings.rbb8.C1_3, packings.rbb8.Cs_4,  packings.rbb8.Cs_5,  packings.rbb8.D3d)
else:
    logger.error("Packing identification can only be done with 4 or 8 objects")
    sys.exit(1)

packing = packings.Packing(matrix)
pline   = ",".join([args.matrix, "NR"] + ["NA" for x in all_rbb])

try:
    threshold     = packing.rigidity_threshold()
    rbb_distances = []
    rbb_best_i    = None
    for rbb_i in range(len(all_rbb)):
        rbb = all_rbb[rbb_i]
        dis = packing.distance(rbb['adj_matrix'], threshold)

        rbb_distances += [dis]
        if rbb_best_i is None or rbb_distances[rbb_best_i] > dis:
            rbb_best_i = rbb_i

    rbb_best = all_rbb[rbb_best_i]['name']
    pline = ",".join([args.matrix, rbb_best] + [f"{x:.9f}" for x in rbb_distances])
    logger.info(f"The closest rigid packing is: {rbb_best}")

except ValueError:
    logger.info(f"This packing is not rigid")

if not args.no_header:
    header_line = ",".join(["name", "rbb"] + [x['name'] for x in all_rbb])

    if args.output is None:
        print(header_line)
    else:
        with open(args.output, "w") as fw:
            fw.write(header_line + "\n")


if args.output is None:
    print(pline)
else:
    with open(args.output, "a") as fa:
        fa.write(pline + "\n")
