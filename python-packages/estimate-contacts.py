#! /usr/bin/env python
import argparse

import interfaces
from shared import logger

parser = argparse.ArgumentParser(description = "Estimate the surface area of contact between cells.")
parser.add_argument("-0", "--exclude-0",                           action  = "store_true",       default = False,     help = "Whether to exclude cell surface area exposed to the background")
parser.add_argument("-r", "--resolution", type = float, nargs = 3, metavar = ("dx", "dy", "dz"), default = (1, 1, 1), help = "Specify the physical size of the voxels.")
parser.add_argument("input",              type = str,                                                                 help = "Input tif or tif.gz file representing labelled cells. Estimation is better with isotropic data.")
parser.add_argument("output",             type = str,                                                                 help = "CSV file where data must be stored.")
args = parser.parse_args()

logger.info("-- CONTACT ESTIMATION --")
logger.info(f"Exclude background: {args.exclude_0}")
logger.info(f"Spatial resolution: {args.resolution}")
logger.info(f"Input data:         {args.input}")
logger.info(f"Output file:        {args.output}")

contacts    = interfaces.Contacts(args.input)
matrix, ids = contacts.compute(return_index = True, weighted = True, spacing = args.resolution, ignore_zero = args.exclude_0)

with open(args.output, "w") as fw:
    fw.write(contacts.toCSV())

