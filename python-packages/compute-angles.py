#! /usr/bin/env python
import argparse
import numpy

import interfaces
from shared import logger

parser = argparse.ArgumentParser(description = "Compute the angle at the interface between two cells and the medium")
parser.add_argument("-s", "--summary", action = "store_true", default = False, help = "Aggregates the data to one statistical line with mean, standard deviation, median, min, max and count.")
parser.add_argument("-r", "--radius", type = int, default = 15, help = "Inclusion radius. All the voxels of the surface not further away than the radius will be taken into account when fitting the plans and computing the angles. Small values will be sensitive to noisy segmentation and aliasing. Big values will over-estimate the angle.")
parser.add_argument("-R", "--exclusion", type = int, default = 20, help = "Exclusion radius. All the points on the line between two cells not further away than this radius will be excluded from the result.")
parser.add_argument("input", type = str, help = "Input tif or tif.gz file representing isotropic labelled cells.")
parser.add_argument("output", type = str, help = "CSV file where data must be stored.")
args = parser.parse_args()

logger.info("-- ANGLE MEASUREMENT --")
logger.info(f"Only summary:     {args.summary}")
logger.info(f"Inclusion radius: {args.radius}")
logger.info(f"Exclusion radius: {args.exclusion}")
logger.info(f"Input data:       {args.input}")
logger.info(f"Output file:      {args.output}")

angles = interfaces.Angles(args.input)
angles.compute(radius = args.radius, exclusion = args.exclusion)

with open(args.output, "w") as fw:
    if args.summary:
        fw.write(
            angles.toCSV(
                aggregate = [
                    {"name": "min",    "func": numpy.min},
                    {"name": "max",    "func": numpy.max},
                    {"name": "mean",   "func": numpy.mean},
                    {"name": "median", "func": numpy.median},
                    {"name": "stdev",  "func": numpy.std},
                    {"name": "count",  "func": len}]))
    else:
        fw.write(angles.toCSV())
