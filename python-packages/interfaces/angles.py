import numpy

from . import common
from .plane import Plane

class Angles:

    def __init__(self, filepath):
        """
        Extract the angles at the interface between two cells exposed
        to the external environment.
        """
        self.filepath = filepath
        self.angles   = []
        self.coords   = []
        self.labels   = []


    def compute(self, radius = 15, exclusion = 20):
        """
        Measure the angle along lines.

        For each point along the line, we select nearby points of `label1` and `label2` that
        belongs the the surfaces. From these points, we extract the normal and compute the angle.
        """

        volume    = self._volume()
        surfaces  = self._surfaces()
        lines     = self._lines()

        # Keep only the interface between cell and medium
        surfaces  = surfaces[(surfaces[:,5:] == 0).any(axis = 1), :]

        radius2   = radius ** 2
        allpoints = numpy.where(volume > 0)
        allpoints = numpy.asarray((*allpoints, volume[allpoints])).T

        self.angles = []
        self.coords = []
        self.labels = []
        for i in range(len(lines)):

            linec     = lines[i, 0:3]
            linel     = lines[i, 5:]
            if (linel != 0).all():
                continue

            linel = linel[linel > 0]
            if len(linel) != 2:
                continue

            label1 = linel[0]
            label2 = linel[1]

            # Exclude point of line interface where a sphere of radius `exclusion` contains
            # more than 2 labels (background is excluded from `allpoints`)
            nearby = numpy.unique(allpoints[numpy.sum((allpoints[:,:3] - linec[::-1]) ** 2, axis = 1) < exclusion ** 2, 3])
            if len(nearby) > 2:
                continue

            surfaces1 = surfaces[surfaces[:,3] == label1, 0:3]
            surfaces2 = surfaces[surfaces[:,3] == label2, 0:3]

            data1, data2 = self.__compute_vicinity_sphere(surfaces1, surfaces2, linec, radius2)
            if data1 is None or data2 is None:
                continue

            centroid1, normal1 = (data1['centroid'], data1['normal'])
            centroid2, normal2 = (data2['centroid'], data2['normal'])

            # Find the direction outward
            tip1 = tuple(numpy.round(centroid1 + 6 * normal1).astype(int))
            try:
                normal1 *= 1 if volume[tip1[::-1]] != label1 else -1
            except IndexError:
                pass

            tip2 = tuple(numpy.round(centroid2 + 6 * normal2).astype(int))
            try:
                normal2 *= 1 if volume[tip2[::-1]] != label2 else -1
            except IndexError:
                pass

            # If outward and inward meets itself, then the point is an
            # abnormal segmentation
            tip1 = tuple(numpy.round(centroid1 + 6 * normal1).astype(int))
            tip2 = tuple(numpy.round(centroid2 + 6 * normal2).astype(int))
            try:
                if volume[tip1[::-1]] == label1 or volume[tip2[::-1]] == label2:
                    continue
            except IndexError:
                pass

            n1_dot_n2 = numpy.dot(normal1, normal2)
            n1_dot_n2 = max(-1, min(1, n1_dot_n2))
            beta      = numpy.arccos(n1_dot_n2)

            self.angles += [beta]
            self.coords += [(*linec,)]
            self.labels += [(label1, label2)]


    def toCSV(self, aggregate = None):
        """
        Generates CSV data with the angles.
        By default, this method return a CSV file content as a string with 4 columns:
        the X, Y, Z coordinates of where the angle was measured, and the angle.

        If aggregate is specified, the method will return only two rows: the header, and the aggreagted data.
        The `aggregate` object must be a list of dict. Each dict has a name (for the header) and an aggregation
        function, respectively with the keys `name` and `func`. The aggregation function must accept one list
        of data, and must return one value.
        """

        count  = len(self.angles)
        rows   = []
        if aggregate is None:
            rows += ["x,y,z,id1,id2,angle"]
        else:
            rows += [",".join([x["name"] for x in aggregate])]

        if aggregate is None:
            for i in range(count):
                x, y, z  = self.coords[i]
                angle    = self.angles[i]
                id1, id2 = self.labels[i]
                rows    += [f"{x},{y},{z},{id1},{id2},{angle}"]
        else:
            rows += [",".join([f"{x['func'](self.angles)}" for x in aggregate])]

        return "\n".join(rows)


    def _volume(self):
        return common.data(self.filepath)['volume']


    def _surfaces(self):
        return common.interface(self.filepath, count = 2)


    def _lines(self):
        return common.interface(self.filepath, count = 3)


    def __compute_vicinity_sphere(self, surfaces1, surfaces2, source, radius2):
        line2srf1 = numpy.sum((surfaces1 - source) ** 2, axis = 1)
        line2srf2 = numpy.sum((surfaces2 - source) ** 2, axis = 1)

        surfpoints1        = surfaces1[line2srf1 < radius2,:]
        centroid1, normal1 = Plane().fit(surfpoints1)
        if normal1 is None:
            return None, None

        surfpoints2        = surfaces2[line2srf2 < radius2,:]
        centroid2, normal2 = Plane().fit(surfpoints2)
        if normal2 is None:
            return None, None

        return ({"centroid": centroid1, "normal": normal1},
                {"centroid": centroid2, "normal": normal2})


