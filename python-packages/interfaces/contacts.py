import numpy
import skimage.measure

from . import common

import logging
logger = logging.getLogger(__name__)

class Contacts:

    def __init__(self, filepath):
        """
        Analyse and extract informations about
        the contacts between objects.
        """
        self.filepath = filepath
        self.matrix   = None
        self.ids      = None
        self._mcube   = {}


    def compute(self, *, return_index = False, weighted = False, spacing = (1.0, 1.0, 1.0), ignore_zero = True):
        """
        Computes the vicinity matrix of the objects. Each row
        corresponds to one object, and each column corresponds
        to the same or another objects. If not weighted, the matrix will
        be filled with ones and zeros, respectively if there is a contact
        between the two objects or not. With weighted set to False, the
        diagonal will always be filled with ones.

        The matrix rows and columns are sorted by natural order
        of the object index.

        @return_index:bool = False
            Returns the row and column indices in the same order as used
            in the matrix.

        @weighted:bool = False
            Estimates the surface of contact between the two objects
            using marching cube and use this value instead of ones to fill
            the matrix.

        @spacing:tuple(float*3) = (1.0, 1.0, 1.0)
            Specifies the unit size of a voxel.

        @ignore_zero:bool = True
            Ignore pixel of value 0 (background).
        """
        logger.info(f"Loading labelled data in memory: {self.filepath}")
        data     = self._volume()

        logger.info(f"Computing surfaces...")
        surfaces = self._surfaces()

        ids      = [x for x in numpy.unique(data) if x > 0 or not ignore_zero]
        numobj   = len(ids)

        for i in ids:
            logger.info(f"Computing marching cube for object index #{i}...")
            self._compute_marching_cube(i, spacing)

        contacts = numpy.vstack((*{tuple(e) for e in surfaces[:,-2:]},
                                 *zip(ids, ids)))

        matrix   = numpy.zeros((numobj, numobj))

        no = 0
        for contact in contacts:
            no += 1
            logger.info(f"Working with contact #{no} out of {len(contacts)} ({no/len(contacts):.2%})")

            if ignore_zero and (contact == 0).any():
                continue

            id1 = ids.index(contact[0])
            id2 = ids.index(contact[1])
            w   = 1

            if weighted:
                if id1 == id2:
                    w = self._get_surface_area(ids[id1], spacing)
                else:
                    w = self._get_contact_area(ids[id1], ids[id2], spacing)

            matrix[id1, id2] = w
            matrix[id2, id1] = w

        self.matrix = matrix
        self.ids    = ids
        if return_index:
            return matrix, ids

        return matrix


    def toCSV(self):
        """
        Generates CSV data with the contacts.
        This method returns a CSV file content as a string with at least 4 columns:
            - the ID of the object in this row
            - the normalized ID of this object (between 1 and `n`)
            - the total surface area of this object
            - `n` columns, with the proportion of its surface involved in the contact
        """

        count = len(self.ids)
        rows  = ["obj_id,obj_name,total_area," + ",".join([f"obj_{x+1}" for x in range(count)])]
        for nid1 in range(count):
            rid = self.ids[nid1]
            row = [rid, f"obj_{nid1+1}", self.matrix[nid1, nid1]]

            contacts = ["NA"] * count
            for nid2 in range(count):
                if nid1 == nid2:
                    continue

                contacts[nid2] = self.matrix[nid1, nid2] / self.matrix[nid1, nid1]

            row  += contacts
            rows += [",".join([f"{x}" for x in row])]

        return "\n".join(rows)


    def _get_contact_area(self, id1, id2, spacing):
        mcube1 = self._compute_marching_cube(id1, spacing)
        mcube2 = self._compute_marching_cube(id2, spacing)

        surfaces  = self._surfaces()
        subset    = surfaces[numpy.logical_and(surfaces[:,5] == id1, surfaces[:,6] == id2), 0:3]
        subset    = subset[:,::-1] * spacing

        d2        = 4 * sum([x**2 for x in spacing])
        mc_vert1  = mcube1['vertices']
        mc_faces1 = mcube1['faces']
        k = numpy.zeros((mc_vert1.shape[0],)) > 0
        for rowi in range(subset.shape[0]):
            wk   = numpy.where(~k)[0]
            subv = mc_vert1[wk,:]

            d = ((subv - subset[rowi,:]) ** 2).sum(axis = 1)
            w = d < d2
            k[wk[w]] = True

        vertices1_2 = numpy.where(k)[0]
        faces1_2    = mc_faces1[numpy.in1d(mc_faces1, vertices1_2).reshape(mc_faces1.shape).all(axis = 1), :]
        area1       = skimage.measure.mesh_surface_area(mc_vert1, faces1_2)

        mc_vert2    = mcube2['vertices']
        mc_faces2   = mcube2['faces']
        k = numpy.zeros((mc_vert2.shape[0],)) > 0
        for rowi in range(subset.shape[0]):
            wk   = numpy.where(~k)[0]
            subv = mc_vert2[wk,:]

            d = ((subv - subset[rowi,:]) ** 2).sum(axis = 1)
            w = d < d2
            k[wk[w]] = True

        vertices2_1 = numpy.where(k)[0]
        faces2_1    = mc_faces2[numpy.in1d(mc_faces2, vertices2_1).reshape(mc_faces2.shape).all(axis = 1), :]
        area2       = skimage.measure.mesh_surface_area(mc_vert2, faces2_1)

        return (area1 + area2) / 2.0


    def _get_surface_area(self, objid, spacing):
        mc = self._compute_marching_cube(objid, spacing)
        return mc['area']


    def _compute_marching_cube(self, objid, spacing):
        if self._mcube.get(objid, None) is not None:
            return self._mcube.get(objid)

        data = self._volume()

        if objid > 0:
            vertices, faces, _, _ = skimage.measure.marching_cubes(data == objid, spacing = spacing)
        else:
            vertices, faces, _, _ = skimage.measure.marching_cubes(data != 0, spacing = spacing)

        total_area            = skimage.measure.mesh_surface_area(vertices, faces)

        self._mcube[objid] = {'vertices': vertices, 'faces': faces, 'area': total_area}
        return self._mcube[objid]


    def _volume(self):
        return common.data(self.filepath)['volume']


    def _surfaces(self):
        return common.interface(self.filepath, count = 2, exact = True)

