import gzip
import imageio.v3 as imageio
import io
import shutil

def open(filepath, binning = 1):
    try:
        with gzip.open(filepath, 'rb') as fr:
            data = fr.read()
            tif  = imageio.imread(data, extension = ".tif")[:, ::binning, ::binning]
    except gzip.BadGzipFile:
        tif = imageio.imread(filepath)[:, ::binning, ::binning]

    return tif


def write(filepath, data):
    if filepath.lower().endswith(".gz"):
        with io.BytesIO() as inmemory:
            imageio.imwrite(inmemory, data, extension = ".tif")
            inmemory.seek(0)

            with gzip.open(filepath, 'wb') as gw:
                shutil.copyfileobj(inmemory, gw)
    else:
        imageio.imwrite(filepath, data)
