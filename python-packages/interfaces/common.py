"""
In this file, we use the keyword `global`.
I know that some people have very strong opinion about it.
Instead of having a class and methods being exported in a
nasty way, just to keep the object `_data` in a `self`
container, I simply use this file as a sort-of-`class`
wrapper, and use the keyword `global` to access variables
*locally* defined in this file, which is exactly what
the keyword `global` is made for.

That being said, I know that the word `global` may trigger
epidermic reactions. I apologize for that.

As a matter of fact, there are good reasons for that, including
the fact that this piece of code is most likely not thread-safe.
"""

import numpy

from . import tiffgz

_data = {}

def data(filepath):
    global _data

    d = _data.get(filepath, None)
    if d is not None:
        return d

    volume = tiffgz.open(filepath)
    _data[filepath] = {'volume': volume}

    return _data[filepath]


def interface(filepath, count, *, exact = False):
    """
    Returns a list of the voxels that belong to an interface
    made of `count` or more objects. The parameter `exact` will
    force the interface to be shared by exactly `count` objects.
    """
    global _data

    ldata = data(filepath)

    if ldata.get('neighbours') is None:
        ldata['neighbours'] = _neighbours(ldata['volume'])
        _data[filepath] = ldata

    if exact:
        return ldata['neighbours'][ldata['neighbours'][:,4] == count, :(5+count)]

    return ldata['neighbours'][ldata['neighbours'][:,4] >= count, :]


def _neighbours(data):
    """
    Counts the different neighbours around each pixels.

    The returned value is a numpy array. The first three columns
    corresponds to the X, Y and Z coordinates of the voxel. The
    fourth column is the value of the voxel, and the fifth column
    the number of different neighbours. The following columns contain
    the ID of the object sharing this interface, or `-1` if none more.
    """
    bordered = numpy.pad(data, pad_width = 1, mode = 'constant', constant_values = 0)
    rows     = []

    coordinates = numpy.where(bordered != 0)
    numvoxels   = len(coordinates[0])
    maxcount    = 0
    for i in range(numvoxels):
        x, y, z = coordinates[2][i], coordinates[1][i], coordinates[0][i]

        current  = bordered[z, y, x]
        selector = ((z-1, z,   z,   z, z,   z,   z+1),
                    (y,   y-1, y,   y, y,   y+1, y),
                    (x,   x,   x-1, x, x+1, x,   x))

        values   = numpy.unique(bordered[selector]).tolist()
        count    = len(values)
        rows    += [[x, y, z, current, count] + values]

        maxcount = max(maxcount, count)

    for i in range(len(rows)):
        rows[i] += [-1] * (maxcount - rows[i][4])

    return numpy.array(rows)
