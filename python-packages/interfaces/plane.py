import numpy

class Plane:

    def __init__(self):
        pass


    def fit(self, points):
        if len(points) < 3:
            return None, None

        centroid = numpy.mean(points, axis = 0)
        centered = points - centroid

        xx = numpy.mean(centered[:,0] * centered[:,0])
        xy = numpy.mean(centered[:,0] * centered[:,1])
        xz = numpy.mean(centered[:,0] * centered[:,2])
        yy = numpy.mean(centered[:,1] * centered[:,1])
        yz = numpy.mean(centered[:,1] * centered[:,2])
        zz = numpy.mean(centered[:,2] * centered[:,2])

        weighted_dir = numpy.asarray([0, 0, 0], dtype = float)

        det_x    = yy * zz - yz * yz
        det_y    = xx * zz - xz * xz
        det_z    = xx * yy - xy * xy
        det_a    = xz * yz - xy * zz
        det_b    = xy * yz - xz * yy
        det_c    = xy * xz - yz * xx

        axis_dir      = numpy.asarray([det_x, det_a, det_b], dtype = float)
        weighted_dir += (-1 if numpy.dot(weighted_dir, axis_dir) < 0 else 1) * (axis_dir * det_x ** 2)

        axis_dir      = numpy.asarray([det_a, det_y, det_c], dtype = float)
        weighted_dir += (-1 if numpy.dot(weighted_dir, axis_dir) < 0 else 1) * (axis_dir * det_y ** 2)

        axis_dir      = numpy.asarray([det_b, det_c, det_z], dtype = float)
        weighted_dir += (-1 if numpy.dot(weighted_dir, axis_dir) < 0 else 1) * (axis_dir * det_z ** 2)

        norm          = numpy.sqrt(numpy.sum(weighted_dir ** 2))
        if numpy.isclose(norm, 0):
            return centroid, None

        normal        = weighted_dir / norm
        return centroid, normal
