#! /usr/bin/env python
import argparse
import numpy

import packings
from shared import logger

parser = argparse.ArgumentParser(description = "Generate seed for Surface Evolver")
parser.add_argument("--threshold", "-t", type = float, required = False, help = "Cell-cell contact threshold for binarizing the adjacency matrix. Defaults to automatic.")
parser.add_argument("matrix",  type = str, help = "Adjacency matrix as output by `estimate-contacts.py`. The matrix is a file with coma-separated columns and newline-separated rows. The first column must be an identifier of the object, or `0` for the background (the background will be ignored). The second and third columns are not used. The n-th column (with n≥4) MUST correspond to the same object as the (n-2)th row (counting the header row). The diagonal (starting at column 4 row 2) should be filled with non-numeric data (typically: NA).")
parser.add_argument("volumes", type = str, help = "Volumes of the cells as output by `compute-volumes.py`. The file is coma-separated columns and newline-separated rows. The first column must be an identifier of the object, or `0` for the background (the background will be ignored). The second column corresponds to the volume. The third to fifth columns correspond to the x, y and z coordinates of the center of the cell.")
parser.add_argument("output",  type = str, help = "FE output file.")
args = parser.parse_args()

logger.info("-- GENERATING SEED --")
logger.info(f"Matrix:      {args.matrix}")
logger.info(f"Volumes:     {args.volumes}")
logger.info(f"Output file: {args.output}")

logger.info("Reading adjacency matrix")
background = None
cells      = {}
with open(args.matrix, "r") as fr:
    lineno = 0
    for line in fr:
        lineno += 1
        if lineno == 1:
            continue

        elements = line.strip().split(",")
        obj_id   = int(elements[0])
        if obj_id == 0:
            background = lineno-2
            continue

        contacts = []
        for x in elements[3:]:
            try:
                contacts += [float(x)]
            except ValueError:
                contacts += [0]

        cells[obj_id] = {"id": obj_id, "contacts": contacts}

if background is not None:
    for i in cells:
        del cells[i]['contacts'][background]

logger.info("Binarising adjacency matrix")
matrix    = [x['contacts'] for x in cells.values()]
threshold = args.threshold
if threshold is None:
    try:
        packing   = packings.Packing(matrix)
        threshold = packing.rigidity_threshold(normalised = False)
    except Exception as ex:
        threshold = 0

binarised = [[1 if x > threshold else 0 for x in cell] for cell in matrix]

logger.info("Construction of the contact graph")
graph = set()
for ci1 in range(len(binarised)):
    for ci2 in range(len(binarised)):
        if binarised[ci1][ci2] == 1:
            graph.add((min(ci1, ci2), max(ci1, ci2)))

logger.info("Reading of the cell volumes")
with open(args.volumes, "r") as fr:
    lineno = 0
    for line in fr:
        lineno += 1
        if lineno == 1:
            continue

        elements = line.strip().split(",")
        obj_id   = int(elements[0])
        if obj_id == 0:
            continue

        cells[obj_id]['volume'] = float(elements[1])
        cells[obj_id]['x']      = float(elements[2])
        cells[obj_id]['y']      = float(elements[3])
        cells[obj_id]['z']      = float(elements[4])

logger.info("Normalisation of the volumes and coordinates")
volume_avg = sum([x['volume'] for x in cells.values()]) / len(cells)
volumes = numpy.asarray([x['volume'] / volume_avg for x in cells.values()])

coords = numpy.asarray([(x['x'], x['y'], x['z']) for x in cells.values()])
coords = coords / (volume_avg ** (1/3))

graph = sorted(list(graph))

logger.info("Generating the seed")
seed = packings.Seed(coords, graph, volumes)
seed.save_fe(args.output)

logger.info(f"Done in {args.output}")
