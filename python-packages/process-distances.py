#! /usr/bin/env python
import argparse
import morphomap
import os
import sys

from shared import logger

parser = argparse.ArgumentParser(description = "Process morphomap distances to generate distance matrix and tSNE projection")
parser.add_argument("-D", "--dimensions", type  = int, default = 2,    required = False, help = "Number of dimensions for the final projection.")
parser.add_argument("-m", "--matrix",     type  = str, default = None,                   help = "CSV file where the distance map should be stored.")
parser.add_argument("-t", "--tsne",       type  = str, default = None,                   help = "CSV file where the tSNE coordinates should be stored.")
parser.add_argument("-s", "--settings",   nargs = "*", default = [],                     help = "Add advanced settings to the morphomap (key=value). See morphomap.core#Morphomap.__init__ for a list of available settings.")
parser.add_argument("path",                                                              help = "Path to a folder containing a list of distance files. A distance file has one header row and three coma-separated columns, giving the name of the first object, the name of the second object and the distance between the two objects.")
args = parser.parse_args()

logger.info("-- PROCESSING MORPHOMAP --")
logger.info(f"Distance matrix: {args.matrix}")
logger.info(f"tSNE projection: {args.tsne}")
logger.info(f" ├─────> Dimensions: {args.dimensions}")
logger.info(f" └─> Other settings: {args.settings}")
logger.info(f"Input folder:    {args.path}")

if args.matrix is None and args.tsne is None:
    logger.warning("Nothing to do.")
    sys.exit(0)

files = [os.path.join(args.path, x) for x in os.listdir(args.path) if not x.startswith(".")]

mmap = morphomap.Morphomap()
mmap.settings(tsne_dimensions = args.dimensions)
mmap.settings(**{k:v for k,v in [x.split("=") for x in args.settings]})

mmap.load_distances(*files)

if args.matrix is not None:
    mmap.save(distance_matrix = args.matrix)

if args.tsne is not None:
    mmap.save(tsne = args.tsne)

