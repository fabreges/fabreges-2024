#### Settings ----
# Dependencies
library(rstudioapi) # Checking current directory
library(ggplot2)    # Good Graphic Plot 2
library(devtools)   # Install fabreges.xxx packages if necessary

# System settings
setwd(dirname(getSourceEditorContext()$path))
if (!"fabreges.generic" %in% installed.packages()[, "Package"]) {
  devtools::install("fabreges.generic")
}

if (!"fabreges.morphomap" %in% installed.packages()[, "Package"]) {
  devtools::install("fabreges.morphomap")
}

output <- "output"

set.seed(sum(strtoi(strsplit("fabreges2024", "")[[1]], 36L)))

# Data settings
embryo.projection <- file.path("data", "morphomaps", "embryos", "tsne_2d.csv")
embryo.dismat     <- file.path("data", "morphomaps", "embryos", "dismat.csv")

file.categories   <- file.path("data", "misc", "categories.csv")
file.angles       <- file.path("data", "misc", "angles.csv")
file.backbones    <- file.path("data", "misc", "backbones.csv")

time.sampling     <- 10
alpha.sampling    <- 10

# Aesthetics settings
PLOT.BASE.SIZE <- 40

PLOT.MORPHOMAP.LINEWIDTH <- 2
PLOT.MORPHOMAP.POINTSIZE <- 4

PLOT.LINEWIDTH <- 2
PLOT.POINTSIZE <- 5

PLOT.TEXT.EARLY  <- "early"
PLOT.TEXT.LATE   <- "late"
PLOT.TEXT.8CELL  <- "8-cell stage"
PLOT.TEXT.ALPHAP <- "α-parameter"

PLOT.TEXT.TOPO_PRO_CTL <- "Topology proportion (%)\nin control embryos"
PLOT.TEXT.TOPO_PRO_MYH <- expression(atop("Topology proportion (%)", "in mMyh9"^"+/-" ~ embryos))
PLOT.TEXT.TOPO_PRO_PAB <- "Topology proportion (%)\nin PAB-treated embryos"

PLOT.TEXT.CATEGORIES <- c("Control"  = "Control",
                          "mMyh9+/-" = expression("mMyh9"^"+/-"),
                          "+PAB"     = "+PAB")

PLOT.TEXT.RBB     <- c("NR"     = "NR",
                       "Others" = "Others",
                       "C1(1)"  = expression("C"["1"] * "(1)"),
                       "C1(2)"  = expression("C"["1"] * "(2)"),
                       "C1(3)"  = expression("C"["1"] * "(3)"),
                       "C2v(1)" = expression("C"["2v"] * "(1)"),
                       "C2v(2)" = expression("C"["2v"] * "(2)"),
                       "Cs(1)"  = expression("C"["s"] * "(1)"),
                       "Cs(2)"  = expression("C"["s"] * "(2)"),
                       "Cs(3)"  = expression("C"["s"] * "(3)"),
                       "Cs(4)"  = expression("C"["s"] * "(4)"),
                       "Cs(5)"  = expression("C"["s"] * "(5)"),
                       "D2d"    = expression("D"["2d"]),
                       "D3d"    = expression("D"["3d"]),
                       "Td"     = expression("T"["d"]))

PLOT.MORPHOMAP.LEGEND.POSITION <- c("top-left", "top-right", "bottom-left", "bottom-right", "none")[3]
PLOT.MORPHOMAP.LEGEND.SIZE     <- 1.3

COLOR.PALETTE.BACKBONES  <- c("D2d"   = "#39CCCC", "Cs(2)"  = "#B10DC9", "Others" = "#AAAAAA", "NR" = "#333333")
COLOR.PALETTE.CATEGORIES <- c("Control" = "#333333", "mMyh9+/-" = "#0B5CD0", "+PAB" = "#6FD3FF")

# Auto settings:
if (PLOT.MORPHOMAP.LEGEND.POSITION == "top-left") {
  PLOT.MORPHOMAP.LEGEND.POSITION        <- "inside"
  PLOT.MORPHOMAP.LEGEND.POSITION.INSIDE <- c(0.02, 0.98)
  PLOT.MORPHOMAP.LEGEND.JUSTIFICATION   <- c(0, 1)
} else if (PLOT.MORPHOMAP.LEGEND.POSITION == "top-right") {
  PLOT.MORPHOMAP.LEGEND.POSITION        <- "inside"
  PLOT.MORPHOMAP.LEGEND.POSITION.INSIDE <- c(0.98, 0.98)
  PLOT.MORPHOMAP.LEGEND.JUSTIFICATION   <- c(1, 1)
} else if (PLOT.MORPHOMAP.LEGEND.POSITION == "bottom-left") {
  PLOT.MORPHOMAP.LEGEND.POSITION        <- "inside"
  PLOT.MORPHOMAP.LEGEND.POSITION.INSIDE <- c(0.02, 0.02)
  PLOT.MORPHOMAP.LEGEND.JUSTIFICATION   <- c(0, 0)
} else if (PLOT.MORPHOMAP.LEGEND.POSITION == "bottom-right") {
  PLOT.MORPHOMAP.LEGEND.POSITION        <- "inside"
  PLOT.MORPHOMAP.LEGEND.POSITION.INSIDE <- c(0.98, 0.02)
  PLOT.MORPHOMAP.LEGEND.JUSTIFICATION   <- c(1, 0)
} else if (PLOT.MORPHOMAP.LEGEND.POSITION == "none") {
  PLOT.MORPHOMAP.LEGEND.POSITION        <- "none"
  PLOT.MORPHOMAP.LEGEND.POSITION.INSIDE <- c(0, 0)
  PLOT.MORPHOMAP.LEGEND.JUSTIFICATION   <- c(0, 0)
}


#### Shared functions ----
facet.labeller <- function(x) {
  ### Labeller of facets for Figure S5B,C,G
  ### `alpha.limits` is (re)defined for each figure

  alpha.l <- round(alpha.limits["low"], 2)
  alpha.u <- round(alpha.limits["high"], 2)

  data.frame(stage = c("uncompacted" = paste0("α > ", alpha.u, " (20% highest)"),
                       "in-between"  = paste0(alpha.l, " ≤ α ≤ ", alpha.u),
                       "compacted"   = paste0("α < ", alpha.l, " (20% lowest)"))[x$stage])
}

#### Figure 5A ----
data             <- fabreges.morphomap::load.projection(embryo.projection, file.categories, file.angles, with.backbones = file.backbones,
                                                        only.categories = c("control", "Myh9+/-", "PAB 10\u03bcM"))
alpha.range      <- range(data$alpha) # Use same axis limits for the three conditions: control, PAB 10\u03bcM treated and mMyh9+/- embryos.
alpha.range.axis <- round(alpha.range / 0.05) * 0.05

data           <- fabreges.morphomap::load.projection(embryo.projection, file.categories, file.angles,
                                                      with.backbones = file.backbones, only.categories = c("control"))
data$rbb.label <- ifelse(data$rbb.label %in% c("D2d", "Cs(2)", "NR"), data$rbb.label, "Others")
distributions  <- data.frame(rbb.label = character(), freq = numeric(), alpha = numeric())

pa <- -1
for (a in seq(alpha.range[1], alpha.range[2], length.out = alpha.sampling)) {
  if (pa == -1) {
    pa <- a
    next()
  }

  subdata <- data[data$alpha > pa & data$alpha <= a, ]
  if (nrow(subdata) == 0) {
    pa <- a
    next()
  }

  distrib <- table(subdata$rbb.label)
  distrib <- data.frame(rbb.label   = names(distrib),
                        freq        = as.numeric(distrib) / sum(distrib),
                        alpha       = mean(max(pa, 0), a))

  distributions <- rbind(distributions, distrib)
  pa            <- a
}

# Add missing topologies, if any (set to 0%)
distributions <- fabreges.generic::grpagg(distributions, "alpha", function(d) {
  for (rbbv in c("D2d", "Cs(2)", "NR", "Others")) {
    if (! rbbv %in% d$rbb) {
      d <- rbind(d,
                 within(d[1, ], {
                   rbb.label <- rbbv
                   freq <- 0
                 }))
    }
  }

  d
})

alpha.breaks <- c(min(alpha.range.axis), mean(alpha.range.axis), max(alpha.range.axis))

g <- ggplot(distributions, aes(x = alpha, y = freq, color = rbb.label, group = rbb.label))
g <- g + geom_line(linewidth = PLOT.LINEWIDTH)
g <- g + scale_color_manual(values = COLOR.PALETTE.BACKBONES, labels = PLOT.TEXT.RBB)
g <- g + scale_y_continuous(PLOT.TEXT.TOPO_PRO_CTL, labels = function(x) paste0(100 * x, "%"), limits = c(0, NA))
g <- g + scale_x_continuous(PLOT.TEXT.ALPHAP, breaks = alpha.breaks, limits = rev(range(alpha.breaks)),
                            trans = scales::trans_new("inv1", function(x) 1 - x, function(x) 1 - x))
g <- g + guides(colour = guide_legend(override.aes = list(linewidth = PLOT.BASE.SIZE / 10)))
g <- g + theme_minimal(base_size = PLOT.BASE.SIZE)
g <- g + theme(legend.position = "top",
               legend.justification = c(1, 1),
               legend.title = element_blank(),
               legend.text = element_text(size = 0.9 * PLOT.BASE.SIZE),
               legend.margin = margin(-0.5 * PLOT.BASE.SIZE + 5, 5, 5, 5),
               panel.grid.minor.y = element_blank(),
               plot.background = element_rect(fill = "white", color = NA))
print(g)

ggsave(file.path(output, "Figure 5A.png"), plot = g, width = 800, height = 800, units = "px", dpi = 100)

#### Figure 5B ----
data <- fabreges.morphomap::load.projection(embryo.projection, file.categories, file.angles, only.categories = c("control", "Myh9+/-"))

g <- ggplot(data, aes(x = norm_time, y = alpha, color = category.name, group = category))
g <- g + geom_line(mapping = aes(group = dataset), alpha = 0.2)
g <- g + geom_smooth(se = FALSE, method = "loess", formula = y ~ x, linewidth = PLOT.LINEWIDTH)
g <- g + scale_x_continuous(name = PLOT.TEXT.8CELL, breaks = c(0, 1), labels = c(PLOT.TEXT.EARLY, PLOT.TEXT.LATE))
g <- g + scale_y_continuous(name = PLOT.TEXT.ALPHAP)
g <- g + scale_color_manual(values = COLOR.PALETTE.CATEGORIES, labels = PLOT.TEXT.CATEGORIES)
g <- g + guides(colour = guide_legend(override.aes = list(linewidth = PLOT.BASE.SIZE / 10)))
g <- g + theme_minimal(base_size = PLOT.BASE.SIZE)
g <- g + theme(legend.position = "inside",
               legend.position.inside = c(0.05, 0.05),
               legend.justification = c(0, 0),
               legend.title = element_blank(),
               legend.text = element_text(hjust = 0),
               axis.text.x = element_text(hjust = c(0, 1)),
               panel.grid.minor.x = element_blank(),
               strip.text = element_blank(),
               plot.background = element_rect(fill = "white", color = NA))

print(g)
ggsave(file.path(output, "Figure 5B.png"), plot = g, width = 800, height = 800, units = "px", dpi = 100)

#### Figure 5D ----
data             <- fabreges.morphomap::load.projection(embryo.projection, file.categories, file.angles, with.backbones = file.backbones,
                                                        only.categories = c("control", "Myh9+/-", "PAB 10\u03bcM"))
alpha.range      <- range(data$alpha) # Use same axis limits for the three conditions: control, PAB 10\u03bcM treated and mMyh9+/- embryos.
alpha.range.axis <- round(alpha.range / 0.05) * 0.05

data           <- fabreges.morphomap::load.projection(embryo.projection, file.categories, file.angles,
                                                      with.backbones = file.backbones, only.categories = c("Myh9+/-"))
data$rbb.label <- ifelse(data$rbb.label %in% c("D2d", "Cs(2)", "NR"), data$rbb.label, "Others")
distributions  <- data.frame(rbb.label = character(), freq = numeric(), alpha = numeric())

pa <- -1
for (a in seq(alpha.range[1], alpha.range[2], length.out = alpha.sampling)) {
  if (pa == -1) {
    pa <- a
    next()
  }

  subdata <- data[data$alpha > pa & data$alpha <= a, ]
  if (nrow(subdata) == 0) {
    pa <- a
    next()
  }

  distrib <- table(subdata$rbb.label)
  distrib <- data.frame(rbb.label   = names(distrib),
                        freq        = as.numeric(distrib) / sum(distrib),
                        alpha       = mean(max(pa, 0), a))

  distributions <- rbind(distributions, distrib)
  pa            <- a
}

# Add missing topologies, if any (set to 0%)
distributions <- fabreges.generic::grpagg(distributions, "alpha", function(d) {
  for (rbbv in c("D2d", "Cs(2)", "NR", "Others")) {
    if (! rbbv %in% d$rbb) {
      d <- rbind(d,
                 within(d[1, ], {
                   rbb.label <- rbbv
                   freq <- 0
                 }))
    }
  }

  d
})

alpha.breaks <- c(min(alpha.range.axis), mean(alpha.range.axis), max(alpha.range.axis))

g <- ggplot(distributions, aes(x = alpha, y = freq, color = rbb.label, group = rbb.label))
g <- g + geom_line(linewidth = PLOT.LINEWIDTH)
g <- g + scale_color_manual(values = COLOR.PALETTE.BACKBONES, labels = PLOT.TEXT.RBB)
g <- g + scale_y_continuous(PLOT.TEXT.TOPO_PRO_MYH, labels = function(x) paste0(100 * x, "%"), limits = c(0, NA))
g <- g + scale_x_continuous(PLOT.TEXT.ALPHAP, breaks = alpha.breaks, limits = rev(range(alpha.breaks)),
                            trans = scales::trans_new("inv1", function(x) 1 - x, function(x) 1 - x))
g <- g + guides(colour = guide_legend(override.aes = list(linewidth = PLOT.BASE.SIZE / 10)))
g <- g + theme_minimal(base_size = PLOT.BASE.SIZE)
g <- g + theme(legend.position = "top",
               legend.justification = c(1, 1),
               legend.title = element_blank(),
               legend.text = element_text(size = 0.9 * PLOT.BASE.SIZE),
               legend.margin = margin(-0.5 * PLOT.BASE.SIZE + 5, 5, 5, 5),
               panel.grid.minor.y = element_blank(),
               plot.background = element_rect(fill = "white", color = NA))
print(g)

ggsave(file.path(output, "Figure 5D.png"), plot = g, width = 800, height = 800, units = "px", dpi = 100)

#### Figure 5E ----
data      <- fabreges.morphomap::load.projection(embryo.projection, file.categories, only.categories = c("control", "Myh9+/-"))
limits.xy <- fabreges.morphomap::limits(data[, c("x", "y")], margin = 0.05, square = TRUE)

g <- ggplot(data, aes(x = x, y = y, color = category.name))
g <- g + geom_density2d(data = subset(data, norm_time == 1 & category == "control"), color = "grey")
g <- g + geom_point(size = PLOT.MORPHOMAP.POINTSIZE)
g <- g + coord_fixed()
g <- g + scale_x_continuous(limits = limits.xy$x)
g <- g + scale_y_continuous(limits = limits.xy$y)
g <- g + scale_color_manual(values = COLOR.PALETTE.CATEGORIES, labels = PLOT.TEXT.CATEGORIES)
g <- g + guides(colour = guide_legend(override.aes = list(size = PLOT.BASE.SIZE * PLOT.MORPHOMAP.LEGEND.SIZE / 4)))
g <- g + theme_minimal(base_size = PLOT.BASE.SIZE)
g <- g + theme(legend.position = PLOT.MORPHOMAP.LEGEND.POSITION,
               legend.position.inside = PLOT.MORPHOMAP.LEGEND.POSITION.INSIDE,
               legend.justification = PLOT.MORPHOMAP.LEGEND.JUSTIFICATION,
               legend.title = element_blank(),
               legend.text = element_text(size = PLOT.BASE.SIZE * PLOT.MORPHOMAP.LEGEND.SIZE, hjust = 0),
               legend.key.width = unit(1.15 * PLOT.BASE.SIZE * PLOT.MORPHOMAP.LEGEND.SIZE, "pt"),
               legend.key.height = unit(PLOT.BASE.SIZE, "pt"),
               legend.background = element_rect(fill = "white", color = "black", linewidth = 0.5),
               panel.grid = element_blank(),
               panel.background = element_rect(color = "black", linewidth = 4),
               plot.margin = unit(c(0, 0, 0, 0), units = "cm"),
               axis.title = element_blank(),
               axis.ticks = element_blank(),
               axis.ticks.length = unit(0, "pt"),
               axis.text = element_blank())

print(g)

ggsave(file.path(output, "Figure 5E.png"), plot = g, width = 1200, height = 1200, units = "px", dpi = 100)

#### Figure S5B ----
data         <- fabreges.morphomap::load.projection(embryo.projection, file.categories, file.angles,
                                                    with.backbones = file.backbones, only.categories = "control")
alpha.limits <- setNames(quantile(data$alpha, probs = c(0.2, 0.8)), c("low", "high"))

rbb.count <- fabreges.generic::grpagg(data, c("category", "rbb.label"), FUN = function(d) {
  rbind(data.frame(category = d$category[1], category.name = d$category.name[1],
                   rbb.label = d$rbb.label[1], count = sum(d$alpha < alpha.limits["low"]),
                   stage = "compacted"),
        data.frame(category = d$category[1], category.name = d$category.name[1],
                   rbb.label = d$rbb.label[1], count = sum(d$alpha > alpha.limits["high"]),
                   stage = "uncompacted"),
        data.frame(category = d$category[1], category.name = d$category.name[1],
                   rbb.label = d$rbb.label[1], count = sum(d$alpha <= alpha.limits["high"] & d$alpha >= alpha.limits["low"]),
                   stage = "in-between"))
})

rbb.count <- fabreges.generic::grpagg(rbb.count, "stage", FUN = function(d) {
  within(d, freq <- count / sum(count))
})

rbb.count$rbb.label.color <- ifelse(rbb.count$rbb.label %in% c("D2d", "Cs(2)", "NR"), rbb.count$rbb.label, "Others")
rbb.count$rbb.label.order <- paste(rbb.count$rbb.label, rbb.count$stage)
rbb.order                 <- aggregate(count ~ rbb.label.order, rbb.count, sum)
rbb.count$rbb.label.order <- factor(rbb.count$rbb.label.order, levels = rbb.order[order(-rbb.order$count), ]$rbb.label.order)
rbb.count$stage           <- factor(rbb.count$stage, levels = c("uncompacted", "in-between", "compacted"))

g <- ggplot(rbb.count, aes(x = rbb.label.order, y = freq, fill = rbb.label.color))
g <- g + scale_fill_manual(values = COLOR.PALETTE.BACKBONES)
g <- g + geom_bar(stat = "identity")
g <- g + scale_x_discrete(labels = function(x) PLOT.TEXT.RBB[gsub("([^ ]+).*", "\\1", x)])
g <- g + scale_y_continuous(name = PLOT.TEXT.TOPO_PRO_CTL, labels = function(x) sprintf("%.0f", 100 * x))
g <- g + facet_wrap(~stage, labeller = facet.labeller, scales = "free_x")
g <- g + theme_minimal(base_size = PLOT.BASE.SIZE)
g <- g + theme(legend.position = "none",
               axis.title.x = element_blank(),
               axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5),
               panel.grid.minor.x = element_blank(),
               plot.background = element_rect(fill = "white", color = NA))

print(g)
ggsave(file.path(output, "Figure S5B.png"), plot = g, width = 2000, height = 800, units = "px", dpi = 100)

#### Figure S5C ----
data         <- fabreges.morphomap::load.projection(embryo.projection, file.categories, file.angles,
                                                    with.backbones = file.backbones, only.categories = "Myh9+/-")
alpha.limits <- setNames(quantile(data$alpha, probs = c(0.2, 0.8)), c("low", "high"))

rbb.count <- fabreges.generic::grpagg(data, c("category", "rbb.label"), FUN = function(d) {
  rbind(data.frame(category = d$category[1], category.name = d$category.name[1],
                   rbb.label = d$rbb.label[1], count = sum(d$alpha < alpha.limits["low"]),
                   stage = "compacted"),
        data.frame(category = d$category[1], category.name = d$category.name[1],
                   rbb.label = d$rbb.label[1], count = sum(d$alpha > alpha.limits["high"]),
                   stage = "uncompacted"),
        data.frame(category = d$category[1], category.name = d$category.name[1],
                   rbb.label = d$rbb.label[1], count = sum(d$alpha <= alpha.limits["high"] & d$alpha >= alpha.limits["low"]),
                   stage = "in-between"))
})

rbb.count <- fabreges.generic::grpagg(rbb.count, "stage", FUN = function(d) {
  within(d, freq <- count / sum(count))
})

rbb.count$rbb.label.color <- ifelse(rbb.count$rbb.label %in% c("D2d", "Cs(2)", "NR"), rbb.count$rbb.label, "Others")
rbb.count$rbb.label.order <- paste(rbb.count$rbb.label, rbb.count$stage)
rbb.order                 <- aggregate(count ~ rbb.label.order, rbb.count, sum)
rbb.count$rbb.label.order <- factor(rbb.count$rbb.label.order, levels = rbb.order[order(-rbb.order$count), ]$rbb.label.order)
rbb.count$stage           <- factor(rbb.count$stage, levels = c("uncompacted", "in-between", "compacted"))

g <- ggplot(rbb.count, aes(x = rbb.label.order, y = freq, fill = rbb.label.color))
g <- g + scale_fill_manual(values = COLOR.PALETTE.BACKBONES)
g <- g + geom_bar(stat = "identity")
g <- g + scale_x_discrete(labels = function(x) PLOT.TEXT.RBB[gsub("([^ ]+).*", "\\1", x)])
g <- g + scale_y_continuous(name = PLOT.TEXT.TOPO_PRO_MYH, labels = function(x) sprintf("%.0f", 100 * x))
g <- g + facet_wrap(~stage, labeller = facet.labeller, scales = "free_x")
g <- g + theme_minimal(base_size = PLOT.BASE.SIZE)
g <- g + theme(legend.position = "none",
               axis.title.x = element_blank(),
               axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5),
               panel.grid.minor.x = element_blank(),
               plot.background = element_rect(fill = "white", color = NA))

print(g)
ggsave(file.path(output, "Figure S5C.png"), plot = g, width = 2000, height = 800, units = "px", dpi = 100)

#### Figure S5D ----
data <- fabreges.morphomap::load.projection(embryo.projection, file.categories,
                                            with.backbones = file.backbones, only.categories = "Myh9+/-")
data$rbb.label <- ifelse(data$rbb.label %in% c("D2d", "Cs(2)", "NR"), data$rbb.label, "Others")

distributions <- data.frame(rbb.label = character(), freq = numeric(), norm_time = numeric())
for (t in seq(time.sampling - 1) - 1) {
  lower.t <- t / (time.sampling - 1)
  upper.t <- (t + 1) / (time.sampling - 1)

  subdata <- data[data$norm_time >= lower.t & data$norm_time <= upper.t, ]
  distrib <- table(subdata$rbb.label)
  distrib <- data.frame(rbb.label = names(distrib),
                        freq      = as.numeric(distrib) / sum(distrib),
                        norm_time = mean(c(lower.t, upper.t)))
  distributions <- rbind(distributions, distrib)
}

g <- ggplot(distributions, aes(x = norm_time, y = freq, color = rbb.label, group = rbb.label))
g <- g + geom_line(linewidth = PLOT.LINEWIDTH)
g <- g + scale_color_manual(values = COLOR.PALETTE.BACKBONES, labels = PLOT.TEXT.RBB)
g <- g + scale_y_continuous(PLOT.TEXT.TOPO_PRO_MYH, labels = function(x) paste0(100 * x, "%"), limits = c(0, NA))
g <- g + scale_x_continuous(PLOT.TEXT.8CELL, breaks = c(0, 1), labels = c(PLOT.TEXT.EARLY, PLOT.TEXT.LATE), limits = c(0, 1))
g <- g + guides(colour = guide_legend(override.aes = list(linewidth = PLOT.BASE.SIZE / 10)))
g <- g + theme_minimal(base_size = PLOT.BASE.SIZE)
g <- g + theme(legend.position = "top",
               legend.justification = c(1, 1),
               legend.title = element_blank(),
               legend.text = element_text(size = 0.9 * PLOT.BASE.SIZE),
               legend.margin = margin(-0.5 * PLOT.BASE.SIZE + 5, 5, 5, 5),
               axis.text.x = element_text(hjust = c(0, 1)),
               panel.grid.minor.y = element_blank(),
               plot.background = element_rect(fill = "white", color = NA))
print(g)
ggsave(file.path(output, "Figure S5D.png"), plot = g, width = 800, height = 800, units = "px", dpi = 100)

#### Figure S5F ----
data <- fabreges.morphomap::load.projection(embryo.projection, file.categories, file.angles,
                                            only.categories = c("control", "Myh9+/-", "PAB 10\u03bcM"))

g <- ggplot(data, aes(x = norm_time, y = alpha, color = category.name, group = category))
g <- g + geom_line(mapping = aes(group = dataset), alpha = 0.2)
g <- g + geom_smooth(se = FALSE, method = "loess", formula = y ~ x, linewidth = PLOT.LINEWIDTH)
g <- g + scale_x_continuous(name = PLOT.TEXT.8CELL, breaks = c(0, 1), labels = c(PLOT.TEXT.EARLY, PLOT.TEXT.LATE))
g <- g + scale_y_continuous(name = PLOT.TEXT.ALPHAP)
g <- g + scale_color_manual(values = COLOR.PALETTE.CATEGORIES, labels = PLOT.TEXT.CATEGORIES)
g <- g + guides(colour = guide_legend(override.aes = list(linewidth = PLOT.BASE.SIZE / 10)))
g <- g + theme_minimal(base_size = PLOT.BASE.SIZE)
g <- g + theme(legend.position = "inside",
               legend.position.inside = c(0.05, 0.05),
               legend.justification = c(0, 0),
               legend.title = element_blank(),
               legend.text = element_text(hjust = 0),
               axis.text.x = element_text(hjust = c(0, 1)),
               panel.grid.minor.x = element_blank(),
               strip.text = element_blank(),
               plot.background = element_rect(fill = "white", color = NA))

print(g)
ggsave(file.path(output, "Figure S5F.png"), plot = g, width = 800, height = 800, units = "px", dpi = 100)

#### Figure S5G ----
data         <- fabreges.morphomap::load.projection(embryo.projection, file.categories, file.angles,
                                                    with.backbones = file.backbones, only.categories = "PAB 10\u03bcM")
alpha.limits <- setNames(quantile(data$alpha, probs = c(0.2, 0.8)), c("low", "high"))

rbb.count <- fabreges.generic::grpagg(data, c("category", "rbb.label"), FUN = function(d) {
  rbind(data.frame(category = d$category[1], category.name = d$category.name[1],
                   rbb.label = d$rbb.label[1], count = sum(d$alpha < alpha.limits["low"]),
                   stage = "compacted"),
        data.frame(category = d$category[1], category.name = d$category.name[1],
                   rbb.label = d$rbb.label[1], count = sum(d$alpha > alpha.limits["high"]),
                   stage = "uncompacted"),
        data.frame(category = d$category[1], category.name = d$category.name[1],
                   rbb.label = d$rbb.label[1], count = sum(d$alpha <= alpha.limits["high"] & d$alpha >= alpha.limits["low"]),
                   stage = "in-between"))
})

rbb.count <- fabreges.generic::grpagg(rbb.count, "stage", FUN = function(d) {
  within(d, freq <- count / sum(count))
})

rbb.count$rbb.label.color <- ifelse(rbb.count$rbb.label %in% c("D2d", "Cs(2)", "NR"), rbb.count$rbb.label, "Others")
rbb.count$rbb.label.order <- paste(rbb.count$rbb.label, rbb.count$stage)
rbb.order                 <- aggregate(count ~ rbb.label.order, rbb.count, sum)
rbb.count$rbb.label.order <- factor(rbb.count$rbb.label.order, levels = rbb.order[order(-rbb.order$count), ]$rbb.label.order)
rbb.count$stage           <- factor(rbb.count$stage, levels = c("uncompacted", "in-between", "compacted"))

g <- ggplot(rbb.count, aes(x = rbb.label.order, y = freq, fill = rbb.label.color))
g <- g + scale_fill_manual(values = COLOR.PALETTE.BACKBONES)
g <- g + geom_bar(stat = "identity")
g <- g + scale_x_discrete(labels = function(x) PLOT.TEXT.RBB[gsub("([^ ]+).*", "\\1", x)])
g <- g + scale_y_continuous(name = PLOT.TEXT.TOPO_PRO_PAB, labels = function(x) sprintf("%.0f", 100 * x))
g <- g + facet_wrap(~stage, labeller = facet.labeller, scales = "free_x")
g <- g + theme_minimal(base_size = PLOT.BASE.SIZE)
g <- g + theme(legend.position = "none",
               axis.title.x = element_blank(),
               axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5),
               panel.grid.minor.x = element_blank(),
               plot.background = element_rect(fill = "white", color = NA))

print(g)
ggsave(file.path(output, "Figure S5G.png"), plot = g, width = 2000, height = 800, units = "px", dpi = 100)

#### Figure S5H ----
data             <- fabreges.morphomap::load.projection(embryo.projection, file.categories,
                                                        file.angles, with.backbones = file.backbones,
                                                        only.categories = c("control", "Myh9+/-", "PAB 10\u03bcM"))
alpha.range      <- range(data$alpha) # Use same axis limits for the three conditions: control, PAB 10\u03bcM treated and mMyh9+/- embryos.
alpha.range.axis <- round(alpha.range / 0.05) * 0.05

data           <- fabreges.morphomap::load.projection(embryo.projection, file.categories, file.angles,
                                                      with.backbones = file.backbones, only.categories = c("PAB 10\u03bcM"))
data$rbb.label <- ifelse(data$rbb.label %in% c("D2d", "Cs(2)", "NR"), data$rbb.label, "Others")
distributions  <- data.frame(rbb.label = character(), freq = numeric(), alpha = numeric())

pa <- -1
for (a in seq(alpha.range[1], alpha.range[2], length.out = alpha.sampling)) {
  if (pa == -1) {
    pa <- a
    next()
  }

  subdata <- data[data$alpha > pa & data$alpha <= a, ]
  if (nrow(subdata) == 0) {
    pa <- a
    next()
  }

  distrib <- table(subdata$rbb.label)
  distrib <- data.frame(rbb.label   = names(distrib),
                        freq        = as.numeric(distrib) / sum(distrib),
                        alpha       = mean(max(pa, 0), a))

  distributions <- rbind(distributions, distrib)
  pa            <- a
}

# Add missing topologies, if any (set to 0%)
distributions <- fabreges.generic::grpagg(distributions, "alpha", function(d) {
  for (rbbv in c("D2d", "Cs(2)", "NR", "Others")) {
    if (! rbbv %in% d$rbb) {
      d <- rbind(d,
                 within(d[1, ], {
                   rbb.label <- rbbv
                   freq <- 0
                 }))
    }
  }

  d
})

alpha.breaks <- c(min(alpha.range.axis), mean(alpha.range.axis), max(alpha.range.axis))

g <- ggplot(distributions, aes(x = alpha, y = freq, color = rbb.label, group = rbb.label))
g <- g + geom_line(linewidth = PLOT.LINEWIDTH)
g <- g + scale_color_manual(values = COLOR.PALETTE.BACKBONES, labels = PLOT.TEXT.RBB)
g <- g + scale_y_continuous(PLOT.TEXT.TOPO_PRO_PAB, labels = function(x) paste0(100 * x, "%"), limits = c(0, NA))
g <- g + scale_x_continuous(PLOT.TEXT.ALPHAP, breaks = alpha.breaks, limits = rev(range(alpha.breaks)),
                            trans = scales::trans_new("inv1", function(x) 1 - x, function(x) 1 - x))
g <- g + guides(colour = guide_legend(override.aes = list(linewidth = PLOT.BASE.SIZE / 10)))
g <- g + theme_minimal(base_size = PLOT.BASE.SIZE)
g <- g + theme(legend.position = "top",
               legend.justification = c(1, 1),
               legend.title = element_blank(),
               legend.text = element_text(size = 0.9 * PLOT.BASE.SIZE),
               legend.margin = margin(-0.5 * PLOT.BASE.SIZE + 5, 5, 5, 5),
               panel.grid.minor.y = element_blank(),
               plot.background = element_rect(fill = "white", color = NA))
print(g)

ggsave(file.path(output, "Figure S5H.png"), plot = g, width = 800, height = 800, units = "px", dpi = 100)


#### Figure S5I ----
data      <- fabreges.morphomap::load.projection(embryo.projection, file.categories,
                                                 only.categories = c("control", "Myh9+/-", "PAB 10\u03bcM"))
limits.xy <- fabreges.morphomap::limits(data[, c("x", "y")], margin = 0.05, square = TRUE)

g <- ggplot(data, aes(x = x, y = y, color = category.name))
g <- g + geom_density2d(data = subset(data, norm_time == 1 & category == "control"), color = "grey")
g <- g + geom_point(size = PLOT.MORPHOMAP.POINTSIZE)
g <- g + coord_fixed()
g <- g + scale_x_continuous(limits = limits.xy$x)
g <- g + scale_y_continuous(limits = limits.xy$y)
g <- g + scale_color_manual(values = COLOR.PALETTE.CATEGORIES, labels = PLOT.TEXT.CATEGORIES)
g <- g + guides(colour = guide_legend(override.aes = list(size = PLOT.BASE.SIZE * PLOT.MORPHOMAP.LEGEND.SIZE / 4)))
g <- g + theme_minimal(base_size = PLOT.BASE.SIZE)
g <- g + theme(legend.position = PLOT.MORPHOMAP.LEGEND.POSITION,
               legend.position.inside = PLOT.MORPHOMAP.LEGEND.POSITION.INSIDE,
               legend.justification = PLOT.MORPHOMAP.LEGEND.JUSTIFICATION,
               legend.title = element_blank(),
               legend.text = element_text(size = PLOT.BASE.SIZE * PLOT.MORPHOMAP.LEGEND.SIZE, hjust = 0),
               legend.key.width = unit(1.15 * PLOT.BASE.SIZE * PLOT.MORPHOMAP.LEGEND.SIZE, "pt"),
               legend.key.height = unit(PLOT.BASE.SIZE, "pt"),
               legend.background = element_rect(fill = "white", color = "black", linewidth = 0.5),
               panel.grid = element_blank(),
               panel.background = element_rect(color = "black", linewidth = 4),
               plot.margin = unit(c(0, 0, 0, 0), units = "cm"),
               axis.title = element_blank(),
               axis.ticks = element_blank(),
               axis.ticks.length = unit(0, "pt"),
               axis.text = element_blank())

print(g)

ggsave(file.path(output, "Figure S5I.png"), plot = g, width = 1200, height = 1200, units = "px", dpi = 100)
